package com.android.intro;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.utils.Constants;


public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    // shared pref mode
    private int PRIVATE_MODE = 0;
    // Shared preferences file name


    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences( Constants.PREF_NAME, PRIVATE_MODE );
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean( Constants.IS_FIRST_TIME_LAUNCH, isFirstTime );
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean( Constants.IS_FIRST_TIME_LAUNCH, true );
    }

}
