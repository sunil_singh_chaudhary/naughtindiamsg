package com.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.android.utils.Constants;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.android.utils.ToasterUtil;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.termsandconditon.ConformationDialog;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class FullMsgViewFragment extends Fragment implements ConformationDialog.onyesDialoclickListner,ToasterUtil.onclickListnerDialog  {
    private TextView fullmsg_textview, date_textv, username_fullmsg_textv;
    private String msg;
    private ImageView fullmsg_like_imgv, fullmsg_dislike_imgv, fullmsg_share_imgv, fullmsg_copy_imgv, fullmsg_delete_imgv, fav_imagev;
    private TextView fullmsg_like_count_textv, fullmsg_dislike_count_textv, fullmsg_fav_count_textv;
    private String author_id, author_profile_pic, author_name, msg_id, created_date;
    private boolean islike, isdislike, isfavourite;
    private int like_total_count, dislike_total_count, fav_total_count;
    private CircleImageView profile_image_fullmsg_circlimagev;
    private boolean flag = false;
    private String favouriteNotFavouriteString;
    private SignUpPrefManager signUpPrefManager;
    private IResult mResultCallback;
    private int currentServerFavouritePositioin = 7;
    private int currentServerDeletePositioin = 10;
    private String likeornotchangetotruetoString;
    LinearLayout delete_layout_linear;
    private int currentServerUnlikeLikePositioin = 6;
    OnBackPressedCallback callback;
    ConformationDialog conformationDialog;
    ToasterUtil toasterUtil;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        String title="Do you want to Delete ??";
        toasterUtil = new ToasterUtil( this );
        // This callback will only be called when MyFragment is at least Started.
        conformationDialog = new ConformationDialog( title, this );
        callback = new OnBackPressedCallback( true ) {
            @Override
            public void handleOnBackPressed() {
                // Handle the back button event
                FragmentManager fm = getFragmentManager();
                if (fm != null) {
                    if (fm.getBackStackEntryCount() > 0) {
                        fm.popBackStack();

                        Log.e( "backpress", "FullMsgViewFragment" );

                    }
                }


            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback( this, callback );
        super.onCreate( savedInstanceState );
    }
    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService( Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isAcceptingText()) {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } else {
            //  writeToLog("Software Keyboard was not shown");
        }

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate( R.layout.fragment_full_msg_view, container, false );
        getActivity().getWindow().setSoftInputMode( WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN );
        init( root );
        hideSoftKeyboard();

        fullmsg_like_imgv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e( "is Lilke click", islike + "" );
                if (islike) {
                    fullmsg_like_imgv.setBackgroundResource( R.drawable.like_ic_icon );
                    fullmsg_like_count_textv.setText( Constants.negativeofTwoValues( fullmsg_like_count_textv.getText().toString() + "" ) );
                    islike = false;
                    likeornotchangetotruetoString = "2";
                    ShowVollyLikeUnlikeDialog( msg_id, likeornotchangetotruetoString, signUpPrefManager.loged_in_userID() );
                } else {
                    likeornotchangetotruetoString = "1";
                    //-----------------send request to server for like---------------------------------------------------
                    ShowVollyLikeUnlikeDialog( msg_id, likeornotchangetotruetoString, signUpPrefManager.loged_in_userID() );

                    fullmsg_like_imgv.setBackgroundResource( R.drawable.like );
                    fullmsg_dislike_imgv.setBackgroundResource( R.drawable.dislike );

                    islike = true;

                    fullmsg_like_count_textv.setText( Constants.sumofTwoValues( fullmsg_like_count_textv.getText().toString() + "" ) );
                    Log.e( "is Lilke/dis click", isdislike + "" );
                    if (isdislike) {
                        fullmsg_dislike_count_textv.setText( Constants.negativeofTwoValues( fullmsg_dislike_count_textv.getText().toString() + "" ) );

                        isdislike = false;
                    }
                }

            }
        } );

        fullmsg_dislike_imgv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e( "is dilke click", isdislike + "" );
                if (isdislike) {

                    fullmsg_dislike_imgv.setBackgroundResource( R.drawable.dislike );
                    fullmsg_dislike_count_textv.setText( Constants.negativeofTwoValues( fullmsg_dislike_count_textv.getText().toString() + "" ) );
                    isdislike = false;
                    likeornotchangetotruetoString = "1";
                    ShowVollyLikeUnlikeDialog( msg_id, likeornotchangetotruetoString, signUpPrefManager.loged_in_userID() );

                } else {
                    //-----------------send request to server for Dislike---------------------------------------------------
                    likeornotchangetotruetoString = "2";
                    ShowVollyLikeUnlikeDialog( msg_id, likeornotchangetotruetoString, signUpPrefManager.loged_in_userID() );

                    fullmsg_dislike_imgv.setBackgroundResource( R.drawable.dislike_ic_icon );
                    fullmsg_like_imgv.setBackgroundResource( R.drawable.like_ic_icon );
                    isdislike = true;
                    fullmsg_dislike_count_textv.setText( Constants.sumofTwoValues( fullmsg_dislike_count_textv.getText().toString() ) + "" );

                    if (islike) {
                        fullmsg_like_count_textv.setText( Constants.negativeofTwoValues( fullmsg_like_count_textv.getText().toString() ) + "" );
                        islike = false;
                    }

                }


            }
        } );

        fullmsg_share_imgv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String CopiedText = fullmsg_textview.getText().toString();
                Intent sendIntent = new Intent();
                sendIntent.setAction( Intent.ACTION_SEND );
                sendIntent.putExtra( Intent.EXTRA_TEXT, CopiedText+"\n\n\n\n\n\n offered By-- "+"https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() );
                sendIntent.setType( "text/plain" );
                startActivity( sendIntent );

            }
        } );

        fav_imagev.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e( "flag", flag + "--isfav--" + isfavourite );

                favInit();

            }
        } );

        delete_layout_linear.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Delete Msg by Login User
                conformationDialog.show( getFragmentManager(), "Text" );

            }
        } );

        fullmsg_copy_imgv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String CopiedText = fullmsg_textview.getText().toString();
                if (CopiedText.equals( "" )) {
                    Toast.makeText( getActivity(), R.string.blanknotcopied, Toast.LENGTH_LONG ).show();
                } else {
                    if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getActivity().getSystemService( Context.CLIPBOARD_SERVICE );
                        clipboard.setText( CopiedText );
                        Toast.makeText( getActivity(), R.string.copied, Toast.LENGTH_LONG ).show();
                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getActivity().getSystemService( Context.CLIPBOARD_SERVICE );
                        android.content.ClipData clip = android.content.ClipData.newPlainText( getString( R.string.copied ), CopiedText );
                        clipboard.setPrimaryClip( clip );
                        Toast.makeText( getActivity(), R.string.copied, Toast.LENGTH_LONG ).show();
                    }
                }


            }
        } );


        return root;
    }

    public void ShowVollyLikeUnlikeDialog(String msg_id, String Likeornot, String userId) {
        try {
            initVolleyLikeUnlikeCallback();
            VolleyService mVolleyService = new VolleyService( mResultCallback, getActivity(), msg_id, Likeornot, userId );
            mVolleyService.postDataVolley( currentServerUnlikeLikePositioin, "" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initVolleyLikeUnlikeCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                try {
                    obj = new JSONObject( response );
                    // Log.e( "login_resp-", response );

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtil.showToast( Constants.NETWORK_ERROR, getActivity() );

                } else if (error instanceof ServerError) {
                    toasterUtil.showToast( Constants.SERVER_ERROR,getActivity());

                } else if (error instanceof NoConnectionError) {
                    toasterUtil.showToast( Constants.NO_INTERNET_CONNECTION,getActivity() );

                } else if (error instanceof TimeoutError) {
                    toasterUtil.showToast( Constants.CONNECTION_TIME_OUT,getActivity() );

                } else {
                    toasterUtil.showToast( Constants.UNKNOWN_ERROR,getActivity() );

                }
            }
        };

    }

    private void favInit() {
        if (isfavourite) {
            fav_imagev.setBackgroundResource( R.drawable.ic_heart );
            favouriteNotFavouriteString = "0";
            isfavourite = false;
            int sum = Integer.parseInt( fullmsg_fav_count_textv.getText().toString() );
            int count = (sum - 1);
            fullmsg_fav_count_textv.setText( count + "" );
        } else {
            fav_imagev.setBackgroundResource( R.drawable.ic_heart_filled );
            favouriteNotFavouriteString = "1";
            isfavourite = true;
            int sum = Integer.parseInt( fullmsg_fav_count_textv.getText().toString() );
            int count = (sum + 1);
            fullmsg_fav_count_textv.setText( count + "" );
        }
        ShowVollyFavouriteDialog( msg_id, favouriteNotFavouriteString, signUpPrefManager.loged_in_userID() );
    }

    private void ShowVollyDeleteDialog(String msg_id, String userId) {
        try {
            initVolleyFavouriteCallback( currentServerDeletePositioin );
            VolleyService mVolleyService = new VolleyService( mResultCallback, getActivity(), msg_id, userId );
            mVolleyService.postDataVolley( currentServerDeletePositioin, "" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ShowVollyFavouriteDialog(String msg_id, String favouriteNotFavouriteString, String userId) {
        Log.e( "FAVUNFAV--", "msg_id---" + msg_id + "FAV--" + favouriteNotFavouriteString + "userId--" + userId );
        try {
            initVolleyFavouriteCallback( 9 );
            VolleyService mVolleyService = new VolleyService( mResultCallback, getActivity(), msg_id, favouriteNotFavouriteString, userId );
            mVolleyService.postDataVolley( currentServerFavouritePositioin, "" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initVolleyFavouriteCallback(int i) {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                try {
                    if (i == currentServerDeletePositioin) {
                        onback();
                    }
                    obj = new JSONObject( response );
                    // Log.e( "login_resp-", response );
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtil.showToast( Constants.NETWORK_ERROR, getActivity() );

                } else if (error instanceof ServerError) {
                    toasterUtil.showToast( Constants.SERVER_ERROR,getActivity());

                } else if (error instanceof NoConnectionError) {
                    toasterUtil.showToast( Constants.NO_INTERNET_CONNECTION,getActivity() );

                } else if (error instanceof TimeoutError) {
                    toasterUtil.showToast( Constants.CONNECTION_TIME_OUT,getActivity() );

                } else {
                    toasterUtil.showToast( Constants.UNKNOWN_ERROR,getActivity() );

                }
            }
        };

    }

    private void onback() {
        Toast.makeText( getActivity(), R.string.deleted, Toast.LENGTH_LONG ).show();
        fullmsg_textview.setText( "" );
        FragmentManager fm = getFragmentManager();
        if (fm != null) {
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();

                Log.e( "backpress", "my back" );

            }
        }
    }

    private void init(View root) {
        signUpPrefManager = new SignUpPrefManager( getActivity() );
        Bundle b = getArguments();
        if (b.getSerializable( Constants.CLICK_MSG_FULLVIEW_KEY ) != null) {
            msg = b.getString( Constants.CLICK_MSG_FULLVIEW_KEY );
        }

        if (b.getSerializable( Constants.AUTHOR_ID_KEY ) != null) {
            author_id = b.getString( Constants.AUTHOR_ID_KEY );
            Log.e( "authhorid--", author_id );
        }
        if (b.getSerializable( Constants.AUTHOR_PIC_KEY ) != null) {
            author_profile_pic = b.getString( Constants.AUTHOR_PIC_KEY );
        }
        if (b.getSerializable( Constants.AUTHOR_Name_KEY ) != null) {
            author_name = b.getString( Constants.AUTHOR_Name_KEY );
        }
        if (b.getSerializable( Constants.MSG_ID_KEY ) != null) {
            msg_id = b.getString( Constants.MSG_ID_KEY );
        }

        if (b.getSerializable( Constants.IS_LIKE_KEY ) != null) {
            islike = b.getBoolean( Constants.IS_LIKE_KEY );
        }

        if (b.getSerializable( Constants.IS_DISLIKE_KEY ) != null) {
            isdislike = b.getBoolean( Constants.IS_DISLIKE_KEY );
        }

        if (b.getSerializable( Constants.IS_FAVOURITE_KEY ) != null) {
            isfavourite = b.getBoolean( Constants.IS_FAVOURITE_KEY );
        }

        if (b.getSerializable( Constants.LIKE_COUNT_KEY ) != null) {
            like_total_count = b.getInt( Constants.LIKE_COUNT_KEY );
        }
        if (b.getSerializable( Constants.DISLIKE_COUNT_KEY ) != null) {
            dislike_total_count = b.getInt( Constants.DISLIKE_COUNT_KEY );
        }
        if (b.getSerializable( Constants.FAVOURITE_COUNT_KEY ) != null) {
            fav_total_count = b.getInt( Constants.FAVOURITE_COUNT_KEY );
        }
        if (b.getSerializable( Constants.AUTHOR_CREATED_DATE_KEY ) != null) {
            created_date = b.getString( Constants.AUTHOR_CREATED_DATE_KEY );
        }

        //----------------------------------SET MSG------------------------------------------------
        fullmsg_textview = root.findViewById( R.id.fullmsg_textv );
        fullmsg_textview.setText( msg );
        //----------------------------------------------------------------------------------


        //---------------------------------------SET TOTAL COUNTS-------------------------------------------

        fullmsg_like_count_textv = root.findViewById( R.id.fullmsg_like_count_textv );
        fullmsg_like_count_textv.setText( "" + like_total_count );
        fullmsg_dislike_count_textv = root.findViewById( R.id.fullmsg_dislike_count_textv );
        fullmsg_dislike_count_textv.setText( "" + dislike_total_count );
        fullmsg_fav_count_textv = root.findViewById( R.id.fullmsg_fav_count_textv );
        fullmsg_fav_count_textv.setText( "" + fav_total_count );

        //----------------------------------------------------------------------------------
        fullmsg_share_imgv = root.findViewById( R.id.fullmsg_share_imgv );
        fullmsg_delete_imgv = root.findViewById( R.id.fullmsg_delete_imgv );
        fullmsg_copy_imgv = root.findViewById( R.id.fullmsg_copy_imgv );
        fullmsg_like_imgv = root.findViewById( R.id.fullmsg_like_imgv );
        fullmsg_dislike_imgv = root.findViewById( R.id.fullmsg_dislike_imgv );
        date_textv = root.findViewById( R.id.date_textv );
        username_fullmsg_textv = root.findViewById( R.id.username_fullmsg_textv );
        profile_image_fullmsg_circlimagev = root.findViewById( R.id.profile_image_fullmsg_circlimagev );
        fav_imagev = root.findViewById( R.id.fav_imagev );

        //----------------------------------------Set Profile data------------------------------------------

        date_textv.setText( created_date + "" );
        username_fullmsg_textv.setText( author_name + "" );
        Glide.with( getActivity() )
                .load( author_profile_pic ) // Remote URL of image.
                .centerCrop()
                .into( profile_image_fullmsg_circlimagev );

        //----------------------------------------------------------------------------------

        delete_layout_linear = root.findViewById( R.id.delete_layout_linear );
        Log.e( "SIGN IN ID--", signUpPrefManager.loged_in_userID() + "" );
        Log.e( "author_id--", author_id + "" );
        Log.e( "author_profile_pic--", author_profile_pic + "" );
        //check author and User are both same means current click post is posted by the Login user then set delete image visible
        if (author_id.equals( signUpPrefManager.loged_in_userID() )) {
            delete_layout_linear.setVisibility( View.VISIBLE );
        }

        if (islike) {
            fullmsg_like_imgv.setBackgroundResource( R.drawable.like );
        } else {
            fullmsg_like_imgv.setBackgroundResource( R.drawable.like_ic_icon );
        }
        if (isdislike) {
            fullmsg_dislike_imgv.setBackgroundResource( R.drawable.dislike_ic_icon );
        } else {
            fullmsg_dislike_imgv.setBackgroundResource( R.drawable.dislike );
        }

        if (isfavourite) {
            fav_imagev.setBackgroundResource( R.drawable.ic_heart_filled );
        } else {
            fav_imagev.setBackgroundResource( R.drawable.ic_heart );
        }

    }

    @Override
    public void onClick(String click) {
        if (click.equals( "yes" )) {
            ShowVollyDeleteDialog( msg_id, signUpPrefManager.loged_in_userID() );
        } else {
            Toast.makeText( getActivity(), R.string.cancel, Toast.LENGTH_LONG ).show();
        }
    }


    //Toast no internet Dialog alert retry button here
    @Override
    public void onClick(View view, Boolean clicktruefalse) {
        if (clicktruefalse){
            favInit();
        }
    }
}
