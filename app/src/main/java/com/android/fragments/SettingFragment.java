package com.android.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.utils.Constants;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.android.utils.ToasterUtil;
import com.android.utils.ToasterUtilsecond;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.thenaughtyindia.android.LoadingScreenView.CustomProgressDialog;
import com.thenaughtyindia.android.LoginActivity;
import com.thenaughtyindia.android.ProfileEditorActivity;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.SignUpActivity;
import com.thenaughtyindia.android.termsandconditon.ConformationDialog;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingFragment extends Fragment implements ConformationDialog.onyesDialoclickListner, ToasterUtil.onclickListnerDialog {
    private LinearLayout change_pswd_layout, delete_account_layout, logout_layout, profile_info_linearlayout_setting,
            login_layout, layout_profile_edit, share_us, rate_us;
    private LinearLayout terms_and_condition_layout, privicy_policie_layout;
    private SignUpPrefManager signUpPrefManager;
    private TextView display_name_setting, user_name_setting,about_us;
    private CircleImageView profile_img;
    private Button login_btn_setting, signUp_btn_setting;
    private Switch notification_Switch;
    private SignUpPrefManager prefManager;
    private ConformationDialog conformationDialog;
    private ProgressBar progressBar;
    private CustomProgressDialog customProgressDialog;
    private IResult mResultCallback;
    private int LoginValueForForgotPswd = 16;
    private EditText old_pswd_edit, new_pswd_edit, conform_newpswd_edit;
    private ToasterUtilsecond toasterUtilsecond;
    WebView webView;
    public SettingFragment() {
    }

    OnBackPressedCallback callback2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu( true );
        toasterUtilsecond = new ToasterUtilsecond( getActivity() );
        callback2 = new OnBackPressedCallback( true ) {
            @Override
            public void handleOnBackPressed() {
                FragmentManager fm = getFragmentManager();
                if (fm != null) {
                    if (fm.getBackStackEntryCount() > 0) {
                        ActivityCompat.finishAffinity( getActivity() );
                        //  refreshList();
                        Log.e( "BAKC PRS", "setting" );
                    }
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback( this, callback2 );
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate( R.layout.fragment_setting, container, false );
        signUpPrefManager = new SignUpPrefManager( getActivity() );
        init( root );

        return root;
    }

    private void init(View root) {
        about_us=root.findViewById( R.id.about_us );
        customProgressDialog = new CustomProgressDialog();
        prefManager = new SignUpPrefManager( getActivity() );
        progressBar = root.findViewById( R.id.progress );
        change_pswd_layout = root.findViewById( R.id.change_pswd_layout );
        delete_account_layout = root.findViewById( R.id.delete_account_layout );
        login_layout = root.findViewById( R.id.login_layout );
        logout_layout = root.findViewById( R.id.logout_layout );
        display_name_setting = root.findViewById( R.id.display_name_setting );
        user_name_setting = root.findViewById( R.id.user_name_setting );
        login_btn_setting = root.findViewById( R.id.login_btn_setting );
        signUp_btn_setting = root.findViewById( R.id.signUp_btn_setting );
        profile_info_linearlayout_setting = root.findViewById( R.id.profile_info_linearlayout_setting );
        layout_profile_edit = root.findViewById( R.id.layout_profile_edit );
        profile_img = root.findViewById( R.id.profile_img );
        notification_Switch = root.findViewById( R.id.notification_Switch );
        share_us = root.findViewById( R.id.share_us_layout );
        rate_us = root.findViewById( R.id.rate_us_layout );
        terms_and_condition_layout = root.findViewById( R.id.terms_and_condition_layout );
        privicy_policie_layout = root.findViewById( R.id.privicy_policie_layout );

        if (prefManager.getwitchStatus()) {
            notification_Switch.setChecked( true );
            Log.e( "switch already on", "oyeah" );
        } else {
            notification_Switch.setChecked( false );
            Log.e( "switch is off", "On kar" );
        }
        notification_Switch.setOnCheckedChangeListener( new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                prefManager.setwitchStatus( isChecked );
            }
        } );
        Glide.with( getActivity() )
                .load( signUpPrefManager.getprofilePhoto() ) // Remote URL of image.
                .circleCrop()
                .placeholder( R.drawable.app_icon )
                .into( profile_img );

        //conformation Dialog
        String title = getString( R.string.do_you_want_to_logout );
        conformationDialog = new ConformationDialog( title, this );

        //check user Sign in Or Not
        boolean signinornot = signUpPrefManager.isFirstTimeSignIn();
        Log.e( "LOG IN", signinornot + "" );
        if (signinornot) {
            change_pswd_layout.setVisibility( View.VISIBLE );
            delete_account_layout.setVisibility( View.VISIBLE );
            logout_layout.setVisibility( View.VISIBLE );
            profile_info_linearlayout_setting.setVisibility( View.VISIBLE );
            login_layout.setVisibility( View.GONE );

            String displayName = signUpPrefManager.LogedDisplayName();
            String logedinUsername = signUpPrefManager.LogedUserName();
            String logedinEmailId = signUpPrefManager.Logedemail_id();

            if (displayName.equals( "" )) {
                display_name_setting.setText( "No Display Name" );
            } else {
                display_name_setting.setText( displayName );
            }
            user_name_setting.setText( logedinUsername );

            Log.e( "logedinUsername--", logedinUsername + "" );

        } else {
            change_pswd_layout.setVisibility( View.GONE );
            delete_account_layout.setVisibility( View.GONE );
            logout_layout.setVisibility( View.GONE );
            profile_info_linearlayout_setting.setVisibility( View.GONE );
            login_layout.setVisibility( View.VISIBLE );
        }
        about_us.setOnClickListener( v -> {
            webView = new WebView(getActivity());
            webView.loadUrl("http://staging.thenaughtyindia.com/about_us.php");

            new MaterialDialog.Builder(getActivity())
                    .title("About Us")
                    .customView(webView,true)
                    .positiveText("OK")
                    .build()
                    .show();
        } );
        rate_us.setOnClickListener( view -> {
            try {
                Uri marketUri = Uri.parse( "market://details?id=" + getActivity().getPackageName() );
                Intent marketIntent = new Intent( Intent.ACTION_VIEW, marketUri );
                startActivity( marketIntent );
            } catch (ActivityNotFoundException e) {
                Uri marketUri = Uri.parse( "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() );
                Intent marketIntent = new Intent( Intent.ACTION_VIEW, marketUri );
                startActivity( marketIntent );
            }
        } );
        share_us.setOnClickListener( view -> {
            Intent intent = new Intent();
            intent.setAction( Intent.ACTION_SEND );
            intent.putExtra( Intent.EXTRA_TEXT, "I suggest this Awwsome app for you : https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() );
            intent.setType( "text/plain" );
            startActivity( intent );
        } );
        layout_profile_edit.setOnClickListener( view -> {
            startActivity( new Intent( getActivity(), ProfileEditorActivity.class ) );

        } );
        login_btn_setting.setOnClickListener( view -> {
            startActivity( new Intent( getActivity(), LoginActivity.class ) );
        } );
        signUp_btn_setting.setOnClickListener( view -> {
            startActivity( new Intent( getActivity(), SignUpActivity.class ) );
        } );
        change_pswd_layout.setOnClickListener( view -> {
            changePasswordDialog( getActivity() );
        } );
        privicy_policie_layout.setOnClickListener( view -> {

            webView = new WebView(getActivity());
            webView.loadUrl("http://staging.thenaughtyindia.com/privacy_policy.php");

            new MaterialDialog.Builder(getActivity())
                    .title("Privacy Policy")
                    .customView(webView,true)
                    .positiveText("Agree")
                    .build()
                    .show();

         /*   Bundle args = new Bundle();
            args.putString( "storeName", "The Naughty India " );
            args.putString( "PrivacyURL", "http://staging.thenaughtyindia.com/privacy_policy.php" );
            PrivacyPolicyDialog dialog = new PrivacyPolicyDialog(0);
            dialog.setArguments( args );
            dialog.show( getFragmentManager(), "Text" );*/

        } );
        terms_and_condition_layout.setOnClickListener( view -> {
            webView = new WebView(getActivity());
            webView.loadUrl("http://staging.thenaughtyindia.com/terms_condition.php");

            new MaterialDialog.Builder(getActivity())
                    .title("Terms And Condition")
                    .customView(webView,true)
                    .positiveText("Agree")
                    .build()
                    .show();

        } );
        logout_layout.setOnClickListener( view -> {
            conformationDialog.show( getFragmentManager(), "Text" );
        } );



    }



    @Override
    public void onResume() {
        Glide.with( getActivity() )
                .load( signUpPrefManager.getprofilePhoto() ) // Remote URL of image.
                .circleCrop()
                .placeholder( R.drawable.app_icon )
                .into( profile_img );
        super.onResume();
    }

    @Override
    public void onClick(String click) {
        Log.e( "CLICK--", click );
        if (click.equals( "yes" )) {
            signUpPrefManager.SignOutPreference();
            change_pswd_layout.setVisibility( View.GONE );
            delete_account_layout.setVisibility( View.GONE );
            profile_info_linearlayout_setting.setVisibility( View.GONE );
            logout_layout.setVisibility( View.GONE );
            login_layout.setVisibility( View.VISIBLE );
            Glide.with( getActivity() )
                    .load( R.drawable.app_icon ) // Remote URL of image.
                    .circleCrop()
                    .placeholder( R.drawable.app_icon )
                    .into( profile_img );
            Toast.makeText( getActivity(), R.string.logoutsuces, Toast.LENGTH_LONG ).show();

        } else {
            Toast.makeText( getActivity(), R.string.cancel, Toast.LENGTH_LONG ).show();
        }

    }


    public void changePasswordDialog(Activity activity) {
        Dialog dialog = new Dialog( activity );
        dialog.requestWindowFeature( Window.FEATURE_NO_TITLE );
        dialog.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable( true );
        dialog.setContentView( R.layout.reset_pswd_lyout );


        old_pswd_edit = dialog.findViewById( R.id.oldpswd_edit );
        new_pswd_edit = dialog.findViewById( R.id.newpswd_edit );
        conform_newpswd_edit = dialog.findViewById( R.id.conform_newpswd_edit );
        Button chnge_pswd_btn = (Button) dialog.findViewById( R.id.chnge_pswd_btn );


        chnge_pswd_btn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                dialog.dismiss();

                serverCall();
            }
        } );


        dialog.show();
    }

    void initVolleyCallbackchagnepswd() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                JSONObject object2 = null;

                String msg = null;
                try {
                    obj = new JSONObject( response );

                    msg = obj.getString( Constants.GET_MESSAGE_FRM_SERVER );
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                customProgressDialog.dialog.dismiss();
                Toast.makeText( getActivity(), msg, Toast.LENGTH_LONG ).show();

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtilsecond.showToast( Constants.NETWORK_ERROR );
                } else if (error instanceof ServerError) {
                    toasterUtilsecond.showToast( Constants.SERVER_ERROR );
                } else if (error instanceof NoConnectionError) {
                    toasterUtilsecond.showToast( Constants.NO_INTERNET_CONNECTION );
                } else if (error instanceof TimeoutError) {
                    toasterUtilsecond.showToast( Constants.CONNECTION_TIME_OUT );
                } else {
                    toasterUtilsecond.showToast( Constants.UNKNOWN_ERROR );
                }

            }
        };
    }

    private void serverCall() {
        String OLD_PSWD = old_pswd_edit.getText().toString();
        String NEW_PSWD = new_pswd_edit.getText().toString();
        String CONFORM_OLD_PSWD = conform_newpswd_edit.getText().toString();
        try {
            customProgressDialog.show( getActivity(), getString( R.string.wait ) );
            initVolleyCallbackchagnepswd();
            Log.e( "YO", "old-" + OLD_PSWD + "new--" + NEW_PSWD + "conf--" + CONFORM_OLD_PSWD );
            VolleyService mVolleyService = new VolleyService( mResultCallback, getActivity(), signUpPrefManager.loged_in_userID(), OLD_PSWD, NEW_PSWD, CONFORM_OLD_PSWD );
            mVolleyService.postDataVolley( LoginValueForForgotPswd, "" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view, Boolean clicktruefalse) {
        if (clicktruefalse) {
            serverCall();
        }
    }
}
