package com.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.adapters.FavListAdapter;
import com.android.adapters.MypostAdapter;
import com.android.utils.Constants;
import com.android.utils.GetMypostFavouriteConstants;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.thenaughtyindia.android.LoadingScreenView.CustomProgressDialog;
import com.thenaughtyindia.android.LoginActivity;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.termsandconditon.SignInConformationDialog;

public class MyPostFragment extends Fragment implements SignInConformationDialog.onyesDialoclickListner, MypostAdapter.onItemClickListner {
    private IResult mResultCallback;
    public static int GETALL_USER_MSG_CURRENT_SERVER = 11;
    private SignUpPrefManager signUpPrefManager;
    private static final int GRIDVIEW_ITEM_ONE_ROW_SIZE = 2;
    private StickyHeaderGridLayoutManager mLayoutManager;
    private RecyclerView mRecycler;
    private FloatingActionButton floatingActionbutton;
    private String defaulfilterClick = Constants.OLD_TO_NEW;
    private EditText edittext_searchbox_mypost;
    private GetMypostFavouriteConstants getMypostFavouriteConstants;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomProgressDialog customProgressDialog;
    private View search_bar_layout_mypost;
    private String searchkeyword = "";
    private SignInConformationDialog signInConformationDialog;
    private ImageView imag_filter;
    private FavListAdapter.onItemClickListner onItemClickListner;
    OnBackPressedCallback callback2;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu( true );
        callback2 = new OnBackPressedCallback( true ) {
            @Override
            public void handleOnBackPressed() {
                FragmentManager fm = getFragmentManager();
                if (fm != null) {
                    if (fm.getBackStackEntryCount() > 0) {
                        ActivityCompat.finishAffinity( getActivity() );
                        //  refreshList();
                        Log.e( "BAcKPRESS", "MypostFrg" );
                    }
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback( this, callback2 );
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        signUpPrefManager = new SignUpPrefManager( getActivity() );
        customProgressDialog = new CustomProgressDialog();
        View root = inflater.inflate( R.layout.fragment_my_post, container, false );


        swipeRefreshLayout = root.findViewById( R.id.swipeview_mypost );
        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchkeyword = "";
                edittext_searchbox_mypost.setText( "" );
                getMypostFavouriteConstants.showVollyGetUserMsg( getMypostFavouriteConstants.defaulPageClick, getMypostFavouriteConstants.searchFilter,
                        searchkeyword );
            }
        } );
        SetUpRecyclerView( root );

        init( root );

        return root;

    }

    private void init(View root) {
        customProgressDialog.show( getActivity(), getString( R.string.wait ) );
        edittext_searchbox_mypost = root.findViewById( R.id.edittext_searchbox );
        signInConformationDialog = new SignInConformationDialog( this );
        //calling volly server
        getMypostFavouriteConstants = new GetMypostFavouriteConstants( onItemClickListner, customProgressDialog, getActivity(),
                mResultCallback, signUpPrefManager.loged_in_userID(), GETALL_USER_MSG_CURRENT_SERVER, mRecycler, 11,
                swipeRefreshLayout );


        floatingActionbutton = root.findViewById( R.id.floatingActionbutton );

        imag_filter = root.findViewById( R.id.imag_filter );
        search_bar_layout_mypost = root.findViewById( R.id.search_bar_layout_mypost );
        if (signUpPrefManager.loged_in_userID().equals( "0" )) {

            search_bar_layout_mypost.setVisibility( View.GONE );
            customProgressDialog.dialog.dismiss();
            signInConformationDialog.show( getFragmentManager(), "Text" );
        } else {
            search_bar_layout_mypost.setVisibility( View.VISIBLE );
            //call server from here
            Log.e( "SEND PAGES DATA-->", "def click->" + getMypostFavouriteConstants.defaulPageClick + "filter->" + getMypostFavouriteConstants.searchFilter );
            getMypostFavouriteConstants.showVollyGetUserMsg( getMypostFavouriteConstants.defaulPageClick, getMypostFavouriteConstants.searchFilter,
                    searchkeyword );


        }
        edittext_searchbox_mypost.setOnEditorActionListener( new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService( Activity.INPUT_METHOD_SERVICE );
                    inputMethodManager.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0 );

                    searchkeyword = edittext_searchbox_mypost.getText().toString();

                    getMypostFavouriteConstants.showVollyGetUserMsg( getMypostFavouriteConstants.defaulPageClick,
                            getMypostFavouriteConstants.searchFilter, searchkeyword );

                    return true;
                }
                return false;
            }
        } );
        floatingActionbutton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // showAlertDialog();
                //  customProgressDialog.show( getActivity() );
                getMypostFavouriteConstants.showAlertDialog();
            }
        } );

        imag_filter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMypostFavouriteConstants.showFilterDialog( getActivity() );
            }
        } );
    }

    private void SetUpRecyclerView(View root) {
        // Setup recycler
        mRecycler = root.findViewById( R.id.post_recycler_view );
        mLayoutManager = new StickyHeaderGridLayoutManager( GRIDVIEW_ITEM_ONE_ROW_SIZE );
        mLayoutManager.setHeaderBottomOverlapMargin( getResources().getDimensionPixelSize( R.dimen.header_shadow_size ) );
        mLayoutManager.offsetChildrenVertical( 20 );

        mRecycler.setItemAnimator( new DefaultItemAnimator() {
            @Override
            public boolean animateRemove(RecyclerView.ViewHolder holder) {
                dispatchRemoveFinished( holder );
                return false;
            }
        } );

        mRecycler.setLayoutManager( mLayoutManager );


        // show floatingaction button when scroll down and up
        mRecycler.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0)
                    floatingActionbutton.hide();
                else if (dy > 0)
                    floatingActionbutton.show();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged( recyclerView, newState );
            }
        } );


    }


    @Override
    public void onClick(String click) {
        Log.e( "onClick MyPostFragment", "Log-in" );
        startActivity( new Intent( getActivity(), LoginActivity.class ) );
    }

    @Override
    public void onClick(View view, String msg_id_selected, String msg) {
        //when click on adapter return value here interface

    }




}
