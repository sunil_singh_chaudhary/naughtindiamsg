package com.android.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.adapters.FavListAdapter;
import com.android.utils.Constants;
import com.android.utils.GetMypostFavouriteConstants;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.android.utils.ToasterUtil;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.thenaughtyindia.android.LoadingScreenView.CustomProgressDialog;
import com.thenaughtyindia.android.LoginActivity;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.termsandconditon.SignInConformationDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class FavouriteFragment extends Fragment implements SignInConformationDialog.onyesDialoclickListner, PopupMenu.OnMenuItemClickListener,
        ToasterUtil.onclickListnerDialog {
    private RecyclerView.LayoutManager fav_layoutManager;
    private static RecyclerView fav_recyclerView;
    private SignUpPrefManager signUpPrefManager;
    private FloatingActionButton floatingActionbutton;
    private GetMypostFavouriteConstants getMypostFavouriteConstants;
    private IResult mResultCallback;
    private int GETALL_USER_MSG_CURRENT_SERVER = 12;
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomProgressDialog customProgressDialog;
    private View search_bar_layout_fav;
    private ImageView imag_filter;
    private SignInConformationDialog signInConformationDialog;
    private String defaultClickPositionDialog = "0";
    private EditText edittext_searchbox;
    private String searchkeyword = "";
    private FavListAdapter.onItemClickListner onItemClickListner;
    private String mSG_ID;
    private CustomProgressDialog cat_dialog;
    private OnBackPressedCallback callback2;
    private ToasterUtil toasterUtil;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu( true );
        toasterUtil = new ToasterUtil( this );
        callback2 = new OnBackPressedCallback( true ) {
            @Override
            public void handleOnBackPressed() {
                FragmentManager fm = getFragmentManager();
                if (fm != null) {
                    if (fm.getBackStackEntryCount() > 0) {
                        ActivityCompat.finishAffinity( getActivity());
                      //  refreshList();
                        Log.e( "BAKC PRS","Fvourite" );
                    }
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback( this, callback2 );
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate( R.layout.fragment_favourite, container, false );
        swipeRefreshLayout = root.findViewById( R.id.swipeview_favourite );
        callBackListnersfromAdapter();
        cat_dialog = new CustomProgressDialog();
        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshList();
            }
        } );
        setRecyclerView( root );
        initViews( root );

        return root;


    }

    private void setRecyclerView(View root) {

        fav_recyclerView = root.findViewById( R.id.favourite_recycler_view );
        fav_recyclerView.setHasFixedSize( true );
        fav_layoutManager = new LinearLayoutManager( getActivity() );
        fav_recyclerView.setLayoutManager( fav_layoutManager );
        fav_recyclerView.setItemAnimator( new DefaultItemAnimator() );
        fav_recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0)
                    floatingActionbutton.hide();
                else if (dy > 0)
                    floatingActionbutton.show();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged( recyclerView, newState );
            }
        } );
    }

    private void initViews(View root) {
        edittext_searchbox = root.findViewById( R.id.edittext_searchbox );
        search_bar_layout_fav = root.findViewById( R.id.search_bar_layout_fav );
        customProgressDialog = new CustomProgressDialog();
        customProgressDialog.show( getActivity(), getString( R.string.wait ) );

        signUpPrefManager = new SignUpPrefManager( getActivity() );
        imag_filter = root.findViewById( R.id.imag_filter );
        signInConformationDialog = new SignInConformationDialog( this );
        floatingActionbutton = root.findViewById( R.id.floatingActionbutton_favourite );

        //sed data to fetch from server
        getMypostFavouriteConstants = new GetMypostFavouriteConstants( onItemClickListner, customProgressDialog, getActivity(), mResultCallback, signUpPrefManager.loged_in_userID(), GETALL_USER_MSG_CURRENT_SERVER, fav_recyclerView, 12, swipeRefreshLayout );


        //check wheather user loged in or not
        if (signUpPrefManager.loged_in_userID().equals( "0" )) {

            search_bar_layout_fav.setVisibility( View.GONE );

            signInConformationDialog.show( getFragmentManager(), "Text" );

            //dialog dismiss
            customProgressDialog.dialog.dismiss();

        } else {

            search_bar_layout_fav.setVisibility( View.VISIBLE );

            getMypostFavouriteConstants.showVollyGetUserMsg( getMypostFavouriteConstants.defaulPageClick, getMypostFavouriteConstants.searchFilter, searchkeyword );
        }

        floatingActionbutton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showAlertDialog();
                getMypostFavouriteConstants.showAlertDialog();
            }
        } );

        imag_filter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getMypostFavouriteConstants.showFilterDialog( getActivity() );
            }
        } );

        edittext_searchbox.setOnEditorActionListener( new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService( Activity.INPUT_METHOD_SERVICE );
                    inputMethodManager.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0 );

                    searchkeyword = edittext_searchbox.getText().toString();

                    getMypostFavouriteConstants.showVollyGetUserMsg( getMypostFavouriteConstants.defaulPageClick,
                            getMypostFavouriteConstants.searchFilter, searchkeyword );

                    return true;
                }
                return false;
            }
        } );
    }

    @Override
    public void onClick(String click) {

        startActivity( new Intent( getActivity(), LoginActivity.class ) );
    }

    private void initVolleyFavouriteCallback(int i) {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                try {
                   /* if (i == currentServerDeletePositioin) {

                        onback();

                    }*/
                    obj = new JSONObject( response );
                    Log.e( "login_resp-", response );
                    cat_dialog.dialog.dismiss();

                    refreshList();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtil.showToast( Constants.NETWORK_ERROR, getActivity() );

                } else if (error instanceof ServerError) {
                    toasterUtil.showToast( Constants.SERVER_ERROR,getActivity());

                } else if (error instanceof NoConnectionError) {
                    toasterUtil.showToast( Constants.NO_INTERNET_CONNECTION,getActivity() );

                } else if (error instanceof TimeoutError) {
                    toasterUtil.showToast( Constants.CONNECTION_TIME_OUT,getActivity() );

                } else {
                    toasterUtil.showToast( Constants.UNKNOWN_ERROR,getActivity() );

                }
            }
        };

    }

    private void refreshList() {
        getMypostFavouriteConstants.defaulPageClick = "0";
        searchkeyword = "";
        edittext_searchbox.setText( "" );
        getMypostFavouriteConstants.showVollyGetUserMsg( getMypostFavouriteConstants.defaulPageClick, getMypostFavouriteConstants.searchFilter,
                searchkeyword );
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.addtounfav_popmenu:
                Log.e( "Selected now msgid-", mSG_ID );
                String favouriteNotFavouriteString = "0";
                int currentServerFavouritePositioin = 7;
                cat_dialog.show( getActivity(), getString( R.string.waits ) );
                Log.e( "FAVUNFAV--", "msg_id---" + mSG_ID + "FAV--" + favouriteNotFavouriteString + "userId--" + signUpPrefManager.loged_in_userID() );
                try {
                    initVolleyFavouriteCallback( 1 );
                    VolleyService mVolleyService = new VolleyService( mResultCallback, getActivity(), mSG_ID, favouriteNotFavouriteString, signUpPrefManager.loged_in_userID() );
                    mVolleyService.postDataVolley( currentServerFavouritePositioin, "" );
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;


        }
        return false;
    }

    private void callBackListnersfromAdapter() {

        onItemClickListner = new FavListAdapter.onItemClickListner() {
            @Override
            public void onClick(View view, String msg_id_selected) {
                openPopMenuList( view, msg_id_selected );
            }
        };
    }

    private void openPopMenuList(View view, String msg_id_selected) {
        Log.e( "Selected view msgid-", msg_id_selected );
        mSG_ID = msg_id_selected;

        //custom background COlor view
        PopupMenu popup = new PopupMenu( new ContextThemeWrapper( getActivity(), R.style.CustomPopupTheme ), view );
        try {
            // Reflection apis to enforce show icon
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals( MessageListningFragment.POPUP_CONSTANT )) {
                    field.setAccessible( true );
                    Object menuPopupHelper = field.get( popup );
                    Class<?> classPopupHelper = Class.forName( menuPopupHelper.getClass().getName() );
                    Method setForceIcons = classPopupHelper.getMethod( MessageListningFragment.POPUP_FORCE_SHOW_ICON, boolean.class );
                    setForceIcons.invoke( menuPopupHelper, true );
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.getMenuInflater().inflate( R.menu.favourite_pop_menu_item, popup.getMenu() );
        popup.setOnMenuItemClickListener( this );
        popup.show();


    }

    @Override
    public void onClick(View view, Boolean clicktruefalse) {
        if (clicktruefalse){
          //Dialog already Dismiss dnt worry
        }
    }
}
