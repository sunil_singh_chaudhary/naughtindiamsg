package com.android.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.adapters.MessagListningAdapter;
import com.android.utils.Constants;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.android.utils.ToasterUtilsecond;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.thenaughtyindia.android.LoadingScreenView.CustomProgressDialog;
import com.thenaughtyindia.android.LoadingScreenView.FlatDialog;
import com.thenaughtyindia.android.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MessageListningFragment extends Fragment implements PopupMenu.OnMenuItemClickListener {
    private static RecyclerView.Adapter msg_adapter;
    private RecyclerView.LayoutManager msg_layoutManager;
    private static RecyclerView msg_recyclerView;
    public static String subCatergoryKey = null;
    private ArrayList<String> msgFromServer = new ArrayList<>();
    private ArrayList<String> author_name_list = new ArrayList<>();
    private ArrayList<String> author_id_list = new ArrayList<>();
    private ArrayList<String> createbyList = new ArrayList<>();
    private IResult mResultCallback;
    private HashMap<String, String> hasmapList;
    private String total_pages, total_records, sub_cat_name;
    private int totalpgs;
    private int postTOserverValue = 3;
    private AlertDialog alert, alert2;
    private ArrayList<String> msg_id_list = new ArrayList<>();
    private ArrayList<Boolean> is_like_List = new ArrayList<>();
    private ArrayList<Boolean> is_dislike_List = new ArrayList<>();
    private ArrayList<Boolean> is_favourite_List = new ArrayList<>();
    private ArrayList<Integer> like_count_List = new ArrayList<>();
    private ArrayList<Integer> dislike_count_List = new ArrayList<>();
    private ArrayList<Integer> favourite_count_List = new ArrayList<>();
    private ArrayList<String> author_photo_List = new ArrayList<>();
    private SignUpPrefManager signUpPrefManager;
    private Menu menu;
    public int GETALLMSG_CURRENT_SERVER = 1;
    private FloatingActionButton floatingActionbutton;
    private MessagListningAdapter.onItemClickListner onItemClickListner;
    private View root;
    public String defaulPageClick = "0";
    private SwipeRefreshLayout swipeRefreshLayout;
    private int currentServerFavouritePositioin = 7;
    private int currentServerReportAbusePositioin = 13;
    public static int checkedItem = 0;
    public static String POPUP_CONSTANT = "mPopup";
    public static String POPUP_FORCE_SHOW_ICON = "setForceShowIcon";
    private IResult mResultCallback_postmsg;
    private  ImageView imag_addpost, imag_filter;
    private CustomProgressDialog cat_dialog;
    private String searchFilter = "old_to_new";
    public static int filtercheckedItem = 0;
    public String defaulfilterClick = "0";
    private String mSG_ID,msg,searchkeyword="";
    private EditText edittext_searchbox;
    ToasterUtilsecond toasterUtilsecond;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu( true );
        toasterUtilsecond=new ToasterUtilsecond( getActivity() );
        signUpPrefManager = new SignUpPrefManager( getActivity() );
        OnBackPressedCallback callback = new OnBackPressedCallback( true ) {
            @Override
            public void handleOnBackPressed() {
                checkedItem = 0;
                // Handle the back button event
                FragmentManager fm = getFragmentManager();
                if (fm != null) {
                    if (fm.getBackStackEntryCount() > 0) {
                       fm.popBackStack();
                        Log.e( "backpress", "MessageListningFragment" );
                    }
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback( this, callback );
        super.onCreate( savedInstanceState );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        root = inflater.inflate( R.layout.fragment_message_listning, container, false );

        cat_dialog = new CustomProgressDialog();


        callBackListnerfromAdapter();

        init( root );

        GetHasmapSubCategory();

        SetRecyclerView( root );

        showVollyTotalMsg( defaulPageClick, searchFilter,searchkeyword );

        swipeRefreshLayout = root.findViewById( R.id.swipeview_messaging );

        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchkeyword="";
                edittext_searchbox.setText( "" );
                showVollyTotalMsg( defaulPageClick, searchFilter,searchkeyword );
            }
        } );
        return root;
    }



    private void init(View root) {
        edittext_searchbox = root.findViewById( R.id.edittext_searchbox );
        floatingActionbutton = root.findViewById( R.id.floatingActionbutton );
        imag_addpost = root.findViewById( R.id.imag_addpost );
        imag_filter = root.findViewById( R.id.imag_filter );
        Bundle b = getArguments();
        if (b.getSerializable( Constants.CLICK_CATEGORY_NAME_KEY ) != null) {
            sub_cat_name = b.getString( Constants.CLICK_CATEGORY_NAME_KEY );
        }
        if (b.getSerializable( Constants.HASHMAP_CAT_KEY ) != null) {
            hasmapList = (HashMap<String, String>) b.getSerializable( Constants.HASHMAP_CAT_KEY );
        }
        edittext_searchbox.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService( Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                    searchkeyword=edittext_searchbox.getText().toString();

                    showVollyTotalMsg( defaulPageClick, searchFilter,searchkeyword );

                    return true;
                }
                return false;
            }
        });
        imag_addpost.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAddMsgToServerDialog();
            }
        } );
        imag_filter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e( "Click", "yo" );
                showFilterDialog();
            }
        } );
    }

    private void callBackListnerfromAdapter() {

        onItemClickListner = new MessagListningAdapter.onItemClickListner() {
            @Override
            public void onClick(View view, String msg_id_selected,String msg) {
                openPopMenuList( view, msg_id_selected,msg );
            }
            @Override
            public void onFullmsgClickView(int pos) {
                enterFullMsgFragment( pos );
            }

        };

    }

    private void GetHasmapSubCategory() {

        //GET SUBCATEGORY KEY FROM HASMAP
        for (Map.Entry entry : hasmapList.entrySet()) {
            if (sub_cat_name.equals( entry.getValue() )) {
                subCatergoryKey = entry.getKey().toString();
                break;
            }
        }
        Log.e( "ID", DynamicFragment.ID + "" );
        Log.e( "sub_ID 2--", subCatergoryKey );
    }

    private void SetRecyclerView(View root) {
        msg_recyclerView = root.findViewById( R.id.msg_recycler_view );
        msg_recyclerView.setHasFixedSize( true );
        msg_layoutManager = new LinearLayoutManager( getActivity() );
        msg_recyclerView.setLayoutManager( msg_layoutManager );
        msg_recyclerView.setItemAnimator( new DefaultItemAnimator() );

        // show floatingaction button when scroll down and up
        msg_recyclerView.addOnScrollListener( new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy < 0)
                    floatingActionbutton.hide();
                else if (dy > 0)
                    floatingActionbutton.show();
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged( recyclerView, newState );
            }
        } );

        floatingActionbutton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();

            }
        } );

    }


    private void showFilterDialog() {

        int click = Integer.parseInt( defaulfilterClick );
        filtercheckedItem = click;
        Log.e( "CLICK Post", click + "" );
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder( getActivity() );

        alertDialog2.setTitle( R.string.chhoserfilter );

        String[] items2 = new String[2];
        items2[0] = "OLD TO NEW";
        items2[1] = "NEW TO OLD";
        alertDialog2.setSingleChoiceItems( items2, filtercheckedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                filtercheckedItem = which;
                defaulfilterClick = which + "";
                Log.e( "CLICK Post 2", filtercheckedItem + "" );
                if (which == 0) {
                    searchFilter = "old_to_new";
                } else if (which == 1) {
                    searchFilter = "new_to_old";
                }
                Log.e( "Data pass", "clickpage-" + defaulPageClick + "searc-filter--" + searchFilter );
                showVollyTotalMsg( defaulPageClick, searchFilter ,searchkeyword);
                alert2.dismiss();
            }
        } );
        alert2 = alertDialog2.create();
        alert2.setCanceledOnTouchOutside( true );
        alert2.show();
    }


    private void showAlertDialog() {
        int click = Integer.parseInt( defaulPageClick );
        checkedItem = click;

        AlertDialog.Builder alertDialog = new AlertDialog.Builder( getActivity() );

        alertDialog.setTitle( R.string.chooserpage );
        totalpgs = Integer.parseInt( total_pages );
        String[] items = new String[totalpgs];
        for (int i = 0; i < totalpgs; i++) {
            items[i] = "Page -" + i;

        }
        alertDialog.setSingleChoiceItems( items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkedItem = which;
                Log.e( "Default checked set", checkedItem + "" );
                String strName = "" + which;
                defaulPageClick = strName;
                Log.e( "Data pass", "clickpage-" + defaulPageClick + "searc-filter--" + searchFilter );
                showVollyTotalMsg( defaulPageClick, searchFilter ,searchkeyword);
                alert.dismiss();
            }
        } );
        alert = alertDialog.create();
        alert.setCanceledOnTouchOutside( true );
        alert.show();
    }

    private void showAddMsgToServerDialog() {
        FlatDialog flatDialog = new FlatDialog( getActivity() );

        flatDialog.setTitle( getString( R.string.postamessage ) )
                .setTitleColor( Color.parseColor( "#000000" ) )
                .setBackgroundColor( Color.parseColor( "#f5f0e3" ) )
                .setLargeTextFieldHint( getString( R.string.writemsghere ) )
                .setLargeTextFieldHintColor( Color.parseColor( "#000000" ) )
                .setLargeTextFieldBorderColor( Color.parseColor( "#000000" ) )
                .setLargeTextFieldTextColor( Color.parseColor( "#000000" ) )
                .setFirstButtonTextColor( Color.parseColor( "#ffffff" ) )
                .isCancelable( true )
                .setFirstButtonText( getString( R.string.done ) )

                .withFirstButtonListner( new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        flatDialog.dismiss();

                        Log.e( "ID", DynamicFragment.ID + "" );
                        Log.e( "sub_ID 2--", subCatergoryKey );
                        String main_id = DynamicFragment.ID;
                        String sb_id = subCatergoryKey;
                        try {

                            SimpleDateFormat targetFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
                            String postedDate = targetFormat.format( new Date() );

                            String msg = flatDialog.getLargeTextField();
                            String title = " ";
                            cat_dialog.dialog.show();

                            postToServerDataVolly();
                            Log.e( "POSTED--", "ID--" + main_id + "--sb_id--" + sb_id + "--date--" + postedDate + "--msg--" + msg + "--title--" + title + "--id--" + signUpPrefManager.loged_in_userID() + "" );
                            VolleyService mVolleyService = new VolleyService( mResultCallback_postmsg, getActivity(), main_id, sb_id, postedDate, msg, title, signUpPrefManager.loged_in_userID() );
                            mVolleyService.postDataVolley( postTOserverValue, "" );


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } )
                .show();

    }

    private void postToServerDataVolly() {

        mResultCallback_postmsg = new IResult() {
            @Override
            public void notifySuccess(String response) {

                cat_dialog.dialog.dismiss();
                JSONObject obj = null;
                try {
                    obj = new JSONObject( response );

                    String status = obj.getString( "status" );

                    if (status.equals( "false" )) {
                        Toast.makeText( getActivity().getApplicationContext(), R.string.cnatbeempty, Toast.LENGTH_LONG ).show();
                        showAddMsgToServerDialog();
                    } else {
                        Toast.makeText( getActivity().getApplicationContext(), R.string.post_success, Toast.LENGTH_LONG ).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();

                Toast.makeText( getActivity().getApplicationContext(), R.string.unsucess, Toast.LENGTH_LONG ).show();

            }
        };
    }

    private void enterFullMsgFragment(int pos) {
        floatingActionbutton.hide();
        Bundle args = new Bundle();
        FullMsgViewFragment fullMsgViewFragment = new FullMsgViewFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        String BACK_STACK_ROOT_TAG = "MessageListning_fragment";
        transaction.addToBackStack( BACK_STACK_ROOT_TAG );
        args.putString( Constants.AUTHOR_ID_KEY, author_id_list.get( pos ) );
        args.putString( Constants.AUTHOR_PIC_KEY, author_photo_List.get( pos ) );
        args.putString( Constants.AUTHOR_Name_KEY, author_name_list.get( pos ) );
        args.putString( Constants.AUTHOR_CREATED_DATE_KEY, createbyList.get( pos ) );
        args.putString( Constants.CLICK_MSG_FULLVIEW_KEY, msgFromServer.get( pos ) );
        args.putString( Constants.MSG_ID_KEY, msg_id_list.get( pos ) );
        args.putBoolean( Constants.IS_LIKE_KEY, is_like_List.get( pos ) );
        args.putBoolean( Constants.IS_DISLIKE_KEY, is_dislike_List.get( pos ) );
        args.putBoolean( Constants.IS_FAVOURITE_KEY, is_favourite_List.get( pos ) );
        args.putInt( Constants.LIKE_COUNT_KEY, like_count_List.get( pos ) );
        args.putInt( Constants.DISLIKE_COUNT_KEY, dislike_count_List.get( pos ) );
        args.putInt( Constants.FAVOURITE_COUNT_KEY, favourite_count_List.get( pos ) );

        fullMsgViewFragment.setArguments( args );
        transaction.replace( R.id.for_fullmsg_container_messaging, fullMsgViewFragment ).commitAllowingStateLoss();

    }

    public void showVollyTotalMsg(String pageClick, String searchFilter,String searchkeyword) {
        cat_dialog.show( getActivity(), getString( R.string.wait ) );
        VolleyService mVolleyService;
        initVolleyCallback();
        try {

            if (signUpPrefManager.loged_in_userID().equals( "0" )) {
                Log.e( "Not Loged", "In-MSG" );
                Log.e( "Page-", "" + pageClick );
                Log.e( "searchFilter-", "" + searchFilter );
                mVolleyService = new VolleyService( getActivity(), subCatergoryKey, pageClick, "", mResultCallback, searchFilter ,searchkeyword);
                mVolleyService.postDataVolley( GETALLMSG_CURRENT_SERVER, subCatergoryKey );
            } else {
                Log.e( "searchFilter-", "" + searchFilter );
                Log.e( "Page-", "" + pageClick );
                mVolleyService = new VolleyService( getActivity(), subCatergoryKey, pageClick, signUpPrefManager.loged_in_userID(), mResultCallback, searchFilter,searchkeyword );

            }
            mVolleyService.postDataVolley( GETALLMSG_CURRENT_SERVER, subCatergoryKey );

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                msgFromServer.clear();
                author_name_list.clear();
                author_id_list.clear();
                createbyList.clear();
                msg_id_list.clear();
                is_like_List.clear();
                is_dislike_List.clear();
                is_favourite_List.clear();
                like_count_List.clear();
                dislike_count_List.clear();
                favourite_count_List.clear();

                JSONObject first_json_object = null;
                JSONObject sec_json_object = null;
                try {
                    first_json_object = new JSONObject( response );
                    total_pages = first_json_object.getString( "total_pages" );
                    total_records = first_json_object.getString( "total_records" );

                    Log.e( "TOTAL pages--", total_pages );

                    JSONArray responseData = first_json_object.getJSONArray( "responseData" );
                    for (int i = 0; i < responseData.length(); i++) {
                        sec_json_object = responseData.getJSONObject( i );
                        String cat_msg_ID = sec_json_object.getString( "id" );
                        String cat_msg = sec_json_object.getString( "message" );
                        String cat_posted_date = sec_json_object.getString( "created_at" );

                        createbyList.add( cat_posted_date );
                        msgFromServer.add( cat_msg );

                        boolean cat_is_like = sec_json_object.getBoolean( "is_like" );
                        boolean cat_is_dislike = sec_json_object.getBoolean( "is_dislike" );
                        boolean cat_is_favourite = sec_json_object.getBoolean( "is_favourite" );
                        int cat_like_count = sec_json_object.getInt( "like_count" );
                        int cat_dislike_count = sec_json_object.getInt( "dislike_count" );
                        int cat_favourite_count = sec_json_object.getInt( "favourite_count" );

                        msg_id_list.add( cat_msg_ID );
                        is_like_List.add( cat_is_like );
                        is_dislike_List.add( cat_is_dislike );
                        is_favourite_List.add( cat_is_favourite );
                        like_count_List.add( cat_like_count );
                        dislike_count_List.add( cat_dislike_count );
                        favourite_count_List.add( cat_favourite_count );

                        JSONObject getUserDetailobject = sec_json_object.getJSONObject( "author_data" );
                        for (int j = 0; j < getUserDetailobject.length(); j++) {

                            String author_id = getUserDetailobject.getString( "id" );
                            String author_name = getUserDetailobject.getString( "name" );
                            String author_profile = getUserDetailobject.getString( "profile_photo" );

                            author_name_list.add( author_name );
                            author_photo_List.add( author_profile );
                            author_id_list.add( author_id );
                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
               //set Actionbar Data her total msg
              //  menu.findItem( R.id.total_records_items ).setTitle( total_records + "" );
                msg_adapter = new MessagListningAdapter( getActivity(), msg_id_list, msgFromServer, author_name_list, createbyList,
                        author_photo_List, onItemClickListner );

                msg_recyclerView.setAdapter( msg_adapter );

                //dismiss CatDialog is showing already
                cat_dialog.dialog.dismiss();
                swipeRefreshLayout.setRefreshing( false );
            }


            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                cat_dialog.dialog.dismiss();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtilsecond.showToast( Constants.NETWORK_ERROR );
                } else if (error instanceof ServerError) {
                    toasterUtilsecond.showToast( Constants.SERVER_ERROR );
                } else if (error instanceof NoConnectionError) {
                    toasterUtilsecond.showToast( Constants.NO_INTERNET_CONNECTION );
                } else if (error instanceof TimeoutError) {
                    toasterUtilsecond.showToast( Constants.CONNECTION_TIME_OUT );
                } else {
                    toasterUtilsecond.showToast( Constants.UNKNOWN_ERROR );
                }
            }
        };
    }


    private void openPopMenuList(View view, String msg_id_selected,String msg) {
        Log.e( "Selected view msgid-", msg_id_selected );
        mSG_ID = msg_id_selected;
        this.msg=msg;
        //custom background COlor view
        PopupMenu popup = new PopupMenu( new ContextThemeWrapper( getActivity(), R.style.CustomPopupTheme ), view );
        try {
            // Reflection apis to enforce show icon
            Field[] fields = popup.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.getName().equals( POPUP_CONSTANT )) {
                    field.setAccessible( true );
                    Object menuPopupHelper = field.get( popup );
                    Class<?> classPopupHelper = Class.forName( menuPopupHelper.getClass().getName() );
                    Method setForceIcons = classPopupHelper.getMethod( POPUP_FORCE_SHOW_ICON, boolean.class );
                    setForceIcons.invoke( menuPopupHelper, true );
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        popup.getMenuInflater().inflate( R.menu.pop_up_menu, popup.getMenu() );
        popup.setOnMenuItemClickListener( this );
        popup.show();


    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.report_abuse_popmenu:
                Log.e( "Selected now msgid-", mSG_ID );
                String report_abuse = "1";

                cat_dialog.show( getActivity(), getString( R.string.waits ) );

                try {
                    initVolleyReportAbuseCallback();
                    VolleyService mVolleyService = new VolleyService( mResultCallback, getActivity(), mSG_ID, report_abuse, signUpPrefManager.loged_in_userID() );
                    mVolleyService.postDataVolley( currentServerReportAbusePositioin, "" );
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.share_popmenu:
                String CopiedText = msg;
                Intent sendIntent = new Intent();
                sendIntent.setAction( Intent.ACTION_SEND );
                sendIntent.putExtra( Intent.EXTRA_TEXT, CopiedText+"\n\n\n\n\n\n offered By-- "+"https://play.google.com/store/apps/details?id=" + getActivity().getPackageName() );
                sendIntent.setType( "text/plain" );
                startActivity( sendIntent );
                break;
            case R.id.askdelete_popmenu:
                Toast.makeText( getActivity(), "You clicked askdelete_popmenu", Toast.LENGTH_SHORT ).show();
                break;

        }
        return false;
    }

    private void initVolleyReportAbuseCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    obj = new JSONObject( response );
                    cat_dialog.dialog.dismiss();
                    String reportmsg = obj.getString( "message" );
                    Toast.makeText( getActivity(), reportmsg, Toast.LENGTH_LONG ).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtilsecond.showToast( Constants.NETWORK_ERROR );
                } else if (error instanceof ServerError) {
                    toasterUtilsecond.showToast( Constants.SERVER_ERROR );
                } else if (error instanceof NoConnectionError) {
                    toasterUtilsecond.showToast( Constants.NO_INTERNET_CONNECTION );
                } else if (error instanceof TimeoutError) {
                    toasterUtilsecond.showToast( Constants.CONNECTION_TIME_OUT );
                } else {
                    toasterUtilsecond.showToast( Constants.UNKNOWN_ERROR );
                }
            }
        };
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflate menu
        menu.clear();
        inflater.inflate( R.menu.post_menu_main, menu );
        this.menu = menu;
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle menu item clicks
        int id = item.getItemId();

        return super.onOptionsItemSelected( item );
    }


}
