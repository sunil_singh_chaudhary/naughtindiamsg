package com.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.adapters.SubCategoryAdapter;
import com.android.utils.Constants;
import com.android.utils.RecyclerItemClickListener;
import com.google.common.collect.Multimap;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.databinding.FragmentDynamicBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DynamicFragment extends Fragment {
    private Multimap<String, String> multiMaphsmap, multiMaphsmap_description_cat;
    private ArrayList<String> idlist;
    private ArrayList<String> subcategoryList;
    private ArrayList<String> subdescriptioinList;
    private static RecyclerView.Adapter sub_cat_adapter;
    private RecyclerView.LayoutManager sub_cat_layoutManager;
    private int Tabpositioin;
    private HashMap<String, String> subcategoryID_HasmapList;

    public static DynamicFragment newInstance() {
        return new DynamicFragment();
    }

    public static String ID;

    FragmentDynamicBinding dynamicBinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        dynamicBinding = FragmentDynamicBinding.inflate( inflater, container, false );
        View root = dynamicBinding.getRoot();

        initViews( root );

        dynamicBinding.myRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener( getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int pos) {
                        enterMessagingFragment( pos );

                    }
                } ) );

        return root;
    }

    private void enterMessagingFragment(int pos) {

        Bundle args = new Bundle();
        MessageListningFragment messageListningFragment = new MessageListningFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.addToBackStack( null );
        args.putString( Constants.CLICK_CATEGORY_NAME_KEY, subcategoryList.get( pos ) );
        args.putSerializable( Constants.HASHMAP_CAT_KEY, subcategoryID_HasmapList );
        messageListningFragment.setArguments( args );
        transaction.replace( R.id.dynamic_container, messageListningFragment, "dynamic_frag" ).commitAllowingStateLoss();

    }

    private void initViews(View view) {


        dynamicBinding.myRecyclerView.setHasFixedSize( true );
        subcategoryList = new ArrayList<>();
        subdescriptioinList = new ArrayList<>();
        sub_cat_layoutManager = new LinearLayoutManager( getActivity() );
        dynamicBinding.myRecyclerView.setLayoutManager( sub_cat_layoutManager );
        dynamicBinding.myRecyclerView.setItemAnimator( new DefaultItemAnimator() );


        Tabpositioin = getArguments().getInt( Constants.TAB_POSITION_KEY );

        subcategoryList.clear();
        subdescriptioinList.clear();
        multiMaphsmap = (Multimap<String, String>) getArguments().getSerializable( Constants.MULTIMAP_KEY );
        multiMaphsmap_description_cat = (Multimap<String, String>) getArguments().getSerializable( Constants.MULTIMAP_DESCRIPTION_KEY );


        idlist = (ArrayList<String>) getArguments().getSerializable( Constants.IDLIST_KEY );
        subcategoryID_HasmapList = (HashMap<String, String>) getArguments().getSerializable( Constants.SUB_CATEGORY_ID_HASHMAP_KEY );

        //get main ID of json here like politics,jokes etc,
        ID = idlist.get( Tabpositioin );

        for (Map.Entry<String, String> entry : multiMaphsmap.entries()) {
            String key = entry.getKey(), value = entry.getValue();
            if (key.equalsIgnoreCase( idlist.get( Tabpositioin ) )) {
                subcategoryList.add( entry.getValue() );
            }
        }
        for (Map.Entry<String, String> entry : multiMaphsmap_description_cat.entries()) {
            String key = entry.getKey(), value = entry.getValue();
            if (key.equalsIgnoreCase( idlist.get( Tabpositioin ) )) {
                subdescriptioinList.add( entry.getValue() );
            }
        }
        sub_cat_adapter = new SubCategoryAdapter( getActivity(), subcategoryList, subdescriptioinList, subcategoryID_HasmapList );
        dynamicBinding.myRecyclerView.setAdapter( sub_cat_adapter );
    }


}