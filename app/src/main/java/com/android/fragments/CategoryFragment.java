package com.android.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.android.adapters.PlansPagerAdapter;
import com.android.utils.Constants;
import com.google.android.material.tabs.TabLayout;
import com.google.common.collect.Multimap;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.NotificationFirebaseActivity;
import com.thenaughtyindia.android.databinding.FragmentCategoryBinding;

import java.util.ArrayList;
import java.util.HashMap;

import am.appwise.components.ni.NoInternetDialog;

public class CategoryFragment extends Fragment {

    private Bundle bundles;
    private ArrayList<String> tabTitle_list, name_list, idlist;
    private Multimap<String, String> multiMaphsmap, multiMaphsmap_description_cat;
    private HashMap<String, String> subcategoryID_HasmapList;
    private PlansPagerAdapter adapter;
    private NoInternetDialog noInternetDialog;
    private FragmentCategoryBinding categoryBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu( true );
        super.onCreate( savedInstanceState );
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        categoryBinding = FragmentCategoryBinding.inflate(inflater, container, false);
        View root=categoryBinding.getRoot();

        init( );

        SetTabLayout();

        SetPageViewer();

        checkInternetAvailibility();

        return root;


    }


    private void SetPageViewer() {

        // viewPager.setOffscreenPageLimit( 4 );
        categoryBinding.containerFrameLayoutViewpager.addOnPageChangeListener( new TabLayout.TabLayoutOnPageChangeListener( categoryBinding.tabLayout ) );
        categoryBinding.tabLayout.setOnTabSelectedListener( new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                categoryBinding.containerFrameLayoutViewpager.setCurrentItem( tab.getPosition() );
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        } );
    }

    @Override
    public void onResume() {
        checkInternetAvailibility();
        super.onResume();
    }

    private void init() {


        tabTitle_list = new ArrayList<>();

        bundles = getArguments();
        if (bundles.getSerializable( Constants.NAME_LIST_KEY ) != null) {
            name_list = (ArrayList<String>) bundles.getSerializable( Constants.NAME_LIST_KEY );
        }
        if (bundles.getSerializable( Constants.IDLIST_CAT_KEY ) != null) {
            idlist = (ArrayList<String>) bundles.getSerializable( Constants.IDLIST_CAT_KEY );
        }
        if (bundles.getSerializable( Constants.MULTIMAP_HASMAP_CAT_KEY ) != null) {
            multiMaphsmap = (Multimap<String, String>) bundles.getSerializable( Constants.MULTIMAP_HASMAP_CAT_KEY );

        }
        if (bundles.getSerializable( Constants.SUBCAT_HASMAP_ID_CAT_KEY ) != null) {
            subcategoryID_HasmapList = (HashMap<String, String>) bundles.getSerializable( Constants.SUBCAT_HASMAP_ID_CAT_KEY );
        }
        if (bundles.getSerializable( Constants.MULTIMAP_DESCRIPTION_CAT_KEY ) != null) {
            multiMaphsmap_description_cat = (Multimap<String, String>) bundles.getSerializable( Constants.MULTIMAP_DESCRIPTION_CAT_KEY );

        }



    }

    private void SetTabLayout() {
        for (int l = 0; l < name_list.size(); l++) {
            categoryBinding.tabLayout.addTab( categoryBinding.tabLayout.newTab().setText( name_list.get( l ) ) );
            tabTitle_list.add( name_list.get( l ) );
        }
        adapter = new PlansPagerAdapter( getChildFragmentManager(), categoryBinding.tabLayout.getTabCount(), tabTitle_list, idlist, multiMaphsmap, multiMaphsmap_description_cat, subcategoryID_HasmapList );
        categoryBinding.containerFrameLayoutViewpager.setAdapter( adapter );

        /* the ViewPager requires a minimum of 1 as OffscreenPageLimit */
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);
        categoryBinding.containerFrameLayoutViewpager.setOffscreenPageLimit( limit );
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflate menu
        inflater.inflate( R.menu.noti_menu, menu );
        super.onCreateOptionsMenu( menu, inflater );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //handle menu item clicks
        int id = item.getItemId();

        if (id == R.id.notification_menu_item) {
            //do your function here
            startActivity( new Intent( getActivity(), NotificationFirebaseActivity.class ) );
        }


        return super.onOptionsItemSelected( item );
    }

    private void checkInternetAvailibility() {

        noInternetDialog = new NoInternetDialog.Builder( getActivity() ).build();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
}





