package com.android.adapters;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.android.fragments.DynamicFragment;
import com.android.utils.Constants;
import com.google.common.collect.Multimap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class PlansPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private ArrayList<String> tabTitle;
    private ArrayList<String> idlist;
    private Multimap<String, String> multiMaphsmap,multiMaphsmap_description_cat;
    private HashMap<String, String> subcategoryID_HasmapList;

    public PlansPagerAdapter(FragmentManager fm, int NumOfTabs, ArrayList<String> tabTitle,
                             ArrayList<String> idlist, Multimap<String, String> multiMaphsmap,Multimap<String,
                             String> multiMaphsmap_description_cat, HashMap<String, String> subcategoryID_HasmapList) {
        super( fm );
        this.mNumOfTabs = NumOfTabs;
        this.tabTitle = tabTitle;
        this.idlist = idlist;
        this.multiMaphsmap = multiMaphsmap;
        this.subcategoryID_HasmapList = subcategoryID_HasmapList;
        this.multiMaphsmap_description_cat=multiMaphsmap_description_cat;

    }

    @Override
    public Fragment getItem(int position) {

        Bundle args = new Bundle();
        args.putInt( Constants.TAB_POSITION_KEY, position );
        args.putSerializable( Constants.MULTIMAP_KEY, (Serializable) multiMaphsmap );
        args.putSerializable( Constants.MULTIMAP_DESCRIPTION_KEY, (Serializable) multiMaphsmap_description_cat );
        args.putSerializable( Constants.IDLIST_KEY, (Serializable) idlist );
        args.putSerializable( Constants.SUB_CATEGORY_ID_HASHMAP_KEY, (Serializable) subcategoryID_HasmapList );
        Fragment frag = DynamicFragment.newInstance();
        frag.setArguments( args );
        return frag;

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}