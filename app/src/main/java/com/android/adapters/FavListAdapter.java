package com.android.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.thenaughtyindia.android.R;

import java.util.ArrayList;

public class FavListAdapter extends RecyclerView.Adapter<FavListAdapter.MyViewHolder> {

    private ArrayList<String> msg_List;
    private ArrayList<Boolean> is_like_List;
    private ArrayList<Boolean> is_dislike_List;
    private ArrayList<Boolean> is_favourite_List;
    private ArrayList<Integer> like_count_List;
    private ArrayList<Integer> dislike_count_List;
    private ArrayList<String> author_photo_List;
    private ArrayList<String> author_name_list;
    private ArrayList<String> createbyList;
    private ArrayList<String> fav_category_name_List;
    private ArrayList<String> fav_subcategory_name_List;
    private ArrayList<String> msgIDList ;
    Context ctx;
    onItemClickListner onItemClickListner;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView msg_textv,posted_date,username_server,category_name_textv,sub_category_name_textv;
        ImageView profile_image;
        LinearLayout more_selection_linearlayout;
        public MyViewHolder(View itemView) {
            super( itemView );
            this.msg_textv = itemView.findViewById( R.id.msg_textv );
            this.posted_date = itemView.findViewById( R.id.posted_date );
            this.username_server = itemView.findViewById( R.id.username_server );
            this.profile_image = itemView.findViewById( R.id.profile_image );

            this.category_name_textv = itemView.findViewById( R.id.category_name_textv );
            this.sub_category_name_textv = itemView.findViewById( R.id.sub_category_name_textv );
            this.more_selection_linearlayout=itemView.findViewById( R.id.more_selection_linearlayout );
        }
    }

    public interface onItemClickListner {

        void onClick(View view,String msg_id_selected);

    }
    public FavListAdapter(Context ctx,onItemClickListner onItemClickListner, ArrayList<String> msgIDList , ArrayList<String> author_photo_List, ArrayList<String> author_name_list,
                          ArrayList<String> createbyList, ArrayList<String> msg_List,
                          ArrayList<Boolean> is_like_List, ArrayList<Integer> like_count_List,
                          ArrayList<Boolean> is_dislike_List,
                          ArrayList<Integer> dislike_count_List, ArrayList<Boolean> is_favourite_List,
                          ArrayList<String> fav_category_name_List,ArrayList<String> fav_subcategory_name_List)
    {
        this.author_photo_List = author_photo_List;
        this.author_name_list = author_name_list;
         this.onItemClickListner=onItemClickListner;
        this.createbyList = createbyList;
        this.msg_List = msg_List;
        this.msgIDList=msgIDList;

        this.is_like_List = is_like_List;
        this.like_count_List = like_count_List;

        this.is_dislike_List = is_dislike_List;
        this.dislike_count_List = dislike_count_List;
        this.is_favourite_List = is_favourite_List;
        this.ctx = ctx;
        this.fav_category_name_List=fav_category_name_List;
        this.fav_subcategory_name_List=fav_subcategory_name_List;
    }

    @Override
    public FavListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.favourite_listning_cardview, parent, false );

        FavListAdapter.MyViewHolder myViewHolder = new FavListAdapter.MyViewHolder( view );
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final FavListAdapter.MyViewHolder holder, final int listPosition) {

        TextView msg_textv = holder.msg_textv;
        TextView posted_date = holder.posted_date;
        TextView username_server = holder.username_server;

        TextView sub_category_name_textv = holder.sub_category_name_textv;
        TextView category_name_textv = holder.category_name_textv;
        LinearLayout more_selection_linearlayout=holder.more_selection_linearlayout;

        ImageView profile_image = holder.profile_image;

        msg_textv.setText( msg_List.get( listPosition ).toString() );
        Log.e("msg-- set--",msg_List.get( listPosition ));

        Glide.with( ctx )
                .load( author_photo_List.get( listPosition ) ) // Remote URL of image.
                .into( profile_image );

        posted_date.setText( createbyList.get( listPosition ).toString() );
        username_server.setText( author_name_list.get( listPosition ).toString() );

        category_name_textv.setText( fav_category_name_List.get( listPosition ) );
        sub_category_name_textv.setText( fav_subcategory_name_List.get( listPosition ) );

        more_selection_linearlayout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e( "msg id is--" ,msgIDList.get( listPosition )+"");
                onItemClickListner.onClick( view,msgIDList.get( listPosition )+"" );
            }
        } );
    }

    @Override
    public int getItemCount() {
        return msg_List.size();
    }
}
