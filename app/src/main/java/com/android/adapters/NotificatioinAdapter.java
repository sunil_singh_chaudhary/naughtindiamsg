package com.android.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.thenaughtyindia.android.AppDatabase;
import com.thenaughtyindia.android.R;
import com.thenaughtyindia.android.UserDao;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NotificatioinAdapter extends RecyclerView.Adapter<NotificatioinAdapter.MyViewHolder> {
    private ArrayList<String> msg_list, jsonformateStringfromserverList;
    Context ctx;
    UserDao userDao;
    private AppDatabase db;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView msg_notificatioin;
        ImageView delete_notification;
        RelativeLayout notifi_main_container_color;

        public MyViewHolder(View itemView) {
            super( itemView );
            this.msg_notificatioin = itemView.findViewById( R.id.msg_notificatioin );
            this.delete_notification = itemView.findViewById( R.id.delete_notification );
            this.notifi_main_container_color = itemView.findViewById( R.id.notifi_main_container_color );

        }
    }

    public NotificatioinAdapter(Context ctx, ArrayList<String> msg_list, ArrayList<String> jsonformateStringfromserverList) {

        this.msg_list = msg_list;
        this.ctx = ctx;
        this.jsonformateStringfromserverList = jsonformateStringfromserverList;
    }

    @Override
    public NotificatioinAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.notification_card_item, parent, false );

        NotificatioinAdapter.MyViewHolder myViewHolder = new NotificatioinAdapter.MyViewHolder( view );
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TextView msg_notificatioin = holder.msg_notificatioin;
        ImageView delete_notification = holder.delete_notification;
        RelativeLayout notifi_main_container_color = holder.notifi_main_container_color;
        int[] androidColors = ctx.getResources().getIntArray( R.array.androidcolors );
        int randomAndroidColor = androidColors[new Random().nextInt( androidColors.length )];


        notifi_main_container_color.setBackgroundColor( randomAndroidColor );
        msg_notificatioin.setText( msg_list.get( position ) );
        delete_notification.setOnClickListener( v -> {

            new detetAsync( (Activity) ctx, position, msg_list.get( position ), msg_list, jsonformateStringfromserverList ).execute();
        } );
    }


    @Override
    public int getItemCount() {
        return msg_list.size();
    }


    public class detetAsync extends AsyncTask<Void, Void, Void> {
        private WeakReference<Activity> weakActivity;
        private AppDatabase db;
        UserDao userDao;
        List<String> msg_list, jsonformateStringfromserverList;
        String msg_;
        int position;

        public detetAsync(Activity activity, int position, String msg_, ArrayList<String> msg_list, ArrayList<String> jsonformateStringfromserverList) {
            weakActivity = new WeakReference<>( activity );
            this.jsonformateStringfromserverList = jsonformateStringfromserverList;
            this.position = position;
            this.msg_ = msg_;
            db = Room.databaseBuilder( activity, AppDatabase.class, "MyNotiModel" ).build();
            this.msg_list = msg_list;

        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            userDao = db.userDao();
            try {
                Log.e( "MSG-", msg_ + "" );
                Log.e( "position-", position + "" );

                userDao.deleteBymsg( jsonformateStringfromserverList.get( position ) );
                msg_list.remove( position );


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            notifyDataSetChanged();
            Toast.makeText( ctx, "Notification removed", Toast.LENGTH_LONG ).show();
            super.onPostExecute( aVoid );
        }
    }
}
