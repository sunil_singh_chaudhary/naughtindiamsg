package com.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.thenaughtyindia.android.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MessagListningAdapter extends RecyclerView.Adapter<MessagListningAdapter.MyViewHolder> {
    onItemClickListner onItemClickListner;
    private ArrayList<String> msgFromServer;
    private ArrayList<String> postedbyList;
    private ArrayList<String> createbyList;
    private Context ctx;
    private ArrayList<String> profile_photo_List;
    private ArrayList<String> msg_id_list;

    public interface onItemClickListner {

        void onClick(View view, String msg_id_selected, String msg);//pass your object types.

        void onFullmsgClickView(int position);//pass your object types.


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView msg_textv, text_total_views, user_name_textv, posted_date_textv;
        private LinearLayout more_selection_linearlayout;
        private CardView card_view_main;
        private CircleImageView profile_image;

        public MyViewHolder(View itemView) {
            super( itemView );
            this.msg_textv = itemView.findViewById( R.id.msg_textv );
            this.posted_date_textv = itemView.findViewById( R.id.posted_date );
            this.user_name_textv = itemView.findViewById( R.id.username_server );
            this.more_selection_linearlayout = itemView.findViewById( R.id.more_selection_linearlayout );
            this.card_view_main = itemView.findViewById( R.id.card_view_main );
            this.profile_image = itemView.findViewById( R.id.profile_image );


        }
    }


    public MessagListningAdapter(Context ctx, ArrayList<String> msg_id_list, ArrayList<String> msgFromServer, ArrayList<String> postedbyList, ArrayList<String> createbyList, ArrayList<String> profile_photo_List, MessagListningAdapter.onItemClickListner onItemClickListner) {
        this.msgFromServer = msgFromServer;
        this.createbyList = createbyList;
        this.postedbyList = postedbyList;
        this.profile_photo_List = profile_photo_List;
        this.ctx = ctx;
        this.onItemClickListner = onItemClickListner;
        this.msg_id_list = msg_id_list;
    }

    @Override
    public MessagListningAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.msg_listning_cardview, parent, false );
        MessagListningAdapter.MyViewHolder myViewHolder = new MessagListningAdapter.MyViewHolder( view );
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MessagListningAdapter.MyViewHolder holder, final int listPosition) {

        TextView text_msg = holder.msg_textv;
        TextView text_date = holder.posted_date_textv;
        TextView username_textv = holder.user_name_textv;

        CardView card_view_main = holder.card_view_main;
        CircleImageView profile_image = holder.profile_image;
        LinearLayout more_selection_linearlayout = holder.more_selection_linearlayout;

        text_msg.setText( msgFromServer.get( listPosition ).replaceAll( "( +)", " " ).trim() );
        text_date.setText( createbyList.get( listPosition ) );
        username_textv.setText( postedbyList.get( listPosition ) );


        Glide.with( ctx ).load( profile_photo_List.get( listPosition ) )
                .into( profile_image );


        more_selection_linearlayout.setOnClickListener( v -> {

            onItemClickListner.onClick( v, msg_id_list.get( listPosition ), msgFromServer.get( listPosition ) );

        } );


        card_view_main.setOnClickListener( v -> {

            onItemClickListner.onFullmsgClickView( listPosition );
        } );


    }

    @Override
    public int getItemCount() {
        return msgFromServer.size();
    }
}
