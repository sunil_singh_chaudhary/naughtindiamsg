package com.android.utils;


import android.util.Log;

public class Constants {

    // public  static String BASE_URL="http://thenaughtyindia.com/webservice/api_list.php/";
    public static String BASE_URL = "http://staging.thenaughtyindia.com/webservice/api_list.php";
    public static String CLICK_CATEGORY_NAME_KEY = "click_category_name";
    public static String HASHMAP_CAT_KEY = "hasmaplist_cat";
    public static String TAB_POSITION_KEY = "position";
    public static String MULTIMAP_KEY = "multiMaphsmap";
    public static String MULTIMAP_DESCRIPTION_KEY = "multiMaphsmap_description_cat";
    public static String IDLIST_KEY = "idlist";
    public static String SUB_CATEGORY_ID_HASHMAP_KEY = "subcategoryID_HasmapList";
    public static String NAME_LIST_KEY = "name_list_cat";
    public static String IDLIST_CAT_KEY = "idlist_cat";
    public static String MULTIMAP_HASMAP_CAT_KEY = "multiMaphsmap_cat";
    public static String SUBCAT_HASMAP_ID_CAT_KEY = "subcategoryID_HasmapList_cat";
    public static String MULTIMAP_DESCRIPTION_CAT_KEY = "multiMaphsmap_description_cat";
    public static String MAIN_ID_KEY = "main_id";
    public static String SUB_ID_CAT_KEY = "sb_id";
    public static final String PREF_NAME = "Screen-welcome";
    public static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    // msg for FullView MSG Fragment constants
    public static final String AUTHOR_ID_KEY = "author_id";
    public static final String AUTHOR_PIC_KEY = "author_profile_photo";
    public static final String AUTHOR_Name_KEY = "author_name";
    public static String CLICK_MSG_FULLVIEW_KEY = "click_msg_fullview";
    public static final String MSG_ID_KEY = "msg_id_key";
    public static final String IS_LIKE_KEY = "is_like_key";
    public static final String IS_DISLIKE_KEY = "is_dislike_key";
    public static final String IS_FAVOURITE_KEY = "is_fav_key";
    public static final String LIKE_COUNT_KEY = "like_count";
    public static final String DISLIKE_COUNT_KEY = "dislike_count";
    public static final String FAVOURITE_COUNT_KEY = "fav_count_key";
    public static final String AUTHOR_CREATED_DATE_KEY = "author_create_date";
    public static  String NO_INTERNET_CONNECTION = "No Internet Connection";
    public static String SERVER_ERROR = "Server Error";
    public static String NETWORK_ERROR = "Network Error";
    public static String UNKNOWN_ERROR = "Unknown Error";
    public static String CONNECTION_TIME_OUT = "Connection Timeout";

    public static String OLD_TO_NEW = "old_to_new";
    public static String NEW_TO_OLD = "new_to_old";
    public static final String PREF_NAME_SIGNUP = "Login_Manager";
    public static final String IS_FIRST_TIME_LOGIN = "IsFirstTimeLogin";
    public static final String IS_SIGNUP_SUCCESFUL = "Issignup_successful";
    public static final String IS_ni_user_id = "pref_ni_user_id";
    public static final String IS_ni_name = "pref_ni_name";
    public static final String IS_ni_username = "pref_ni_username";
    public static final String IS_ni_email = "pref_ni_email";
    public static final String IS_ni_user_type = "pref_ni_user_type";
    public static final String IS_ni_user_profile_pic = "pref_ni_user_profile_pic";
    public static final String ANDROID_SWITCH_BUTTON_NOTIFICATIOIN = "on_or_off_switch_notification";

    //LOGIN SCREEN SHAREDPREF DATA
    public static final String ni_user_id = "ni_user_id";
    public static final String ni_name = "ni_name";
    public static final String ni_username = "ni_username";
    public static final String ni_email = "ni_email";
    public static final String ni_user_type = "ni_user_type";
    public static final int LOGIN_FROM_SERVER_VALUE = 4;
    public static final int LOGIN_VALUE_FOR_FORGOT_PSWD = 15;
    public static final String IS_LOGIN_SUCCESSFUL_VALUE =  "Login successful";
    public static final String GET_MESSAGE_FRM_SERVER =  "message";
    public static final String GET_STATUS_FRM_SERVER =  "status";
    public static final String GET_RESPONSE_FRM_SERVER =  "responseData";

    //Values FOR PROFILE EDITORS SCREEN
    public static final String USER_ID =  "user_id";
    public static final String USER_TYPE =  "user_type";
    public static final String AGE =  "age";
    public static final String NAME =  "name";
    public static final String EMAIL =  "email";
    public static final String PROFILE_PHOTO =  "profile_photo";
    public static final String GENDER =  "gender";
    public static final String ABOUT_ME =  "about_me";
    public static final String MY_TOTAL_POST_COUNT =  "my_total_post_count";
    public static final String MY_TOTAL_FAVOURITE_COUNT =  "my_total_favourite_count";

    //Values FOR FIRST ACTIVITY SCREEN
    public static final String CATEGORY_NAME =  "category_name";
    public static final String ID =  "id";
    public static final String SUB_CAT_LIST =  "subcategory_list";
    public static final String CATEGORY_ID =  "category_id";
    public static final String SUB_CAT_NAME =  "subcategory_name";
    public static final String DESCRIPTION =  "description";

    //GetMYPOSTFAVOURITE UTILS CONSTANTS
    public static final String IS_LIKE =  "is_like";
    public static final String IS_DISLIKE =  "is_dislike";
    public static final String IS_FAVOURITE =  "is_favourite";
    public static final String IS_LIKE_COUNT =  "like_count";
    public static final String IS_DISLIKE_COUNT =  "dislike_count";
    public static final String IS_CREATED_AT =  "created_at";
    public static final String IS_FAVOURITE_COUNT =  "favourite_count";
    public static final String IS_CATEGORY =  "category";
    public static final String IS_SUBCATEGORY =  "subcategory";
    public static final String IS_TOTAL_PAGES =  "total_pages";
    public static final String IS_TOTAL_RECORDS =  "total_records";
    public static final String IS_AUTHOR_DATE =  "author_data";


    public static String sumofTwoValues(String a) {
        int countlikes = Integer.parseInt( a );
        int sum = (countlikes + 1);
        if (sum<=0){
            return 0+"";
        }
        String tmpStr10 = String.valueOf(sum);
        Log.e( "return posite", tmpStr10 + "" );
        return tmpStr10;
    }

    public static String negativeofTwoValues(String a) {
        int countdislikes = Integer.parseInt( a );

        int minusvalues = (countdislikes -1);
        if (minusvalues<=0){
            return 0+"";
        }
        String tmpStr10 = String.valueOf(minusvalues);
        Log.e( "return negative", tmpStr10 + "" );
        return tmpStr10;
    }



}
