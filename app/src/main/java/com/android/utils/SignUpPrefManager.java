package com.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class SignUpPrefManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;


    public SignUpPrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences( Constants.PREF_NAME_SIGNUP, PRIVATE_MODE );
        editor = pref.edit();
    }

    public void setFirstTimeSignIn(boolean isFirstTime) {
        Log.e( "SGIN pre save-",isFirstTime+"" );
        editor.putBoolean( Constants.IS_FIRST_TIME_LOGIN, isFirstTime );
        editor.commit();
    }
    public void setwitchStatus(boolean isswitchon) {
        Log.e( "IS_switch_on-",isswitchon+"" );
        editor.putBoolean( Constants.ANDROID_SWITCH_BUTTON_NOTIFICATIOIN, isswitchon );
        editor.commit();
    }
    public Boolean getwitchStatus() {

        return pref.getBoolean( Constants.ANDROID_SWITCH_BUTTON_NOTIFICATIOIN,false );
    }
    public  void setprofilePhoto(String path){
        Log.e( "path set--" ,path);
        editor.putString( Constants.IS_ni_user_profile_pic, path );
        editor.commit();
    }
    public String getprofilePhoto() {
        Log.e( "path get--" ,pref.getString( Constants.IS_ni_user_profile_pic,"0" ));
        return pref.getString( Constants.IS_ni_user_profile_pic,"0" );
    }

    public  void SignOutPreference(){
        pref.edit().remove(Constants.IS_FIRST_TIME_LOGIN).commit();
        pref.edit().remove(Constants.IS_ni_user_id).commit();
        pref.edit().remove(Constants.IS_ni_name).commit();
        pref.edit().remove(Constants.IS_ni_username).commit();
        pref.edit().remove(Constants.IS_ni_email).commit();
        pref.edit().remove(Constants.IS_ni_user_type).commit();
    }

    public boolean isFirstTimeSignIn() {
        return pref.getBoolean( Constants.IS_FIRST_TIME_LOGIN, false );
    }
    public String loged_in_userID() {
        return pref.getString( Constants.IS_ni_user_id,"0" );
    }
    public String getSignIn_userName() {
        return pref.getString( Constants.IS_ni_email,"0" );
    }

    public String LogedUserName() {
        return pref.getString( Constants.IS_ni_username,"0" );
    }
    public String LogedDisplayName() {
        return pref.getString( Constants.IS_ni_name,"0" );
    }

    public String Logedemail_id() {
        return pref.getString( Constants.IS_ni_email,"0" );
    }

    public void setFirstTimeSignInDATA(String ni_user_id, String ni_name, String ni_username, String ni_email, String ni_user_type) {
        Log.e("User LOGIN Pref ","ni_user_id-"+ni_user_id+"ni_name-"+ni_name+"ni_username-"+ni_username+"ni_email-"+ni_email+"ni_user_type-"+ni_user_type);
        editor.putString( Constants.IS_ni_user_id, ni_user_id );
        editor.putString( Constants.IS_ni_name, ni_name );
        editor.putString( Constants.IS_ni_username, ni_username );
        editor.putString( Constants.IS_ni_email, ni_email );
        editor.putString( Constants.IS_ni_user_type, ni_user_type );
        editor.commit();
    }

    public void setFirstTimeSignUp(boolean isFirstTimesignup, String ni_user_id, String ni_name, String ni_username, String ni_email, String ni_user_type) {
        Log.e("SIGNUP Pref Save --","isFirstTimesignup-"+isFirstTimesignup+"ni_user_id-"+ni_user_id+"ni_name-"+ni_name+"ni_username-"+ni_username+"ni_email-"+ni_email+"ni_user_type-"+ni_user_type);
        editor.putBoolean( Constants.IS_SIGNUP_SUCCESFUL, isFirstTimesignup );
        editor.putString( Constants.IS_ni_user_id, ni_user_id );
        editor.putString( Constants.IS_ni_name, ni_name );
        editor.putString( Constants.IS_ni_username, ni_username );
        editor.putString( Constants.IS_ni_email, ni_email );
        editor.putString( Constants.IS_ni_user_type, ni_user_type );
        editor.commit();
    }
}
