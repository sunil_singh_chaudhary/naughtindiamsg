package com.android.utils;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class MyNotiModel {

    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "userid")
    public String userid;

    @ColumnInfo(name = "msg")
    public String msg;


    public MyNotiModel(String userid, String msg) {
        this.userid = userid;
        this.msg = msg;
    }
}