package com.android.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class VolleyService {

    IResult mResultCallback = null;
    public Context mContext;
    private String main_id, sb_id;
    private String created_date, posted_by_user_id;
    private String password_or_userid, username_or_msgid, displayName_usrid;
    private String ConfrmPswd, likeornot_Fav, pageNo, msg, title;
    private String msg_id, userId, loggedin_user_id, subCatergoryKeyID, loged_in_userID;
    private String u_name, u_email, u_gender, u_bio;
    private String searchFilter, defaulPageClick, searchkeyword;

    public VolleyService(IResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;
    }

    public VolleyService(IResult resultCallback, Context context, String username_or_msgid, String password_or_userid) {
        mResultCallback = resultCallback;
        mContext = context;
        this.username_or_msgid = username_or_msgid;
        this.password_or_userid = password_or_userid;
    }

    /* public VolleyService(String searchFilter,IResult resultCallback, Context context, String username_or_msgid_defaultpageclick, String password_or_userid) {
         mResultCallback = resultCallback;
         mContext = context;
         this.username_or_msgid_defaultpageclick = username_or_msgid_defaultpageclick;
         this.password_or_userid = password_or_userid;
         this. searchFilter= searchFilter;
     }*/
    public VolleyService(String searchFilter, IResult resultCallback, Context ctx, String defaulPageClick, String loged_in_userID, String searchkeyword) {
        this.mResultCallback = resultCallback;
        mContext = ctx;
        this.defaulPageClick = defaulPageClick;
        this.loged_in_userID = loged_in_userID;
        this.searchFilter = searchFilter;
        this.searchkeyword = searchkeyword;
    }

    public VolleyService(Context context, String subCatergoryKeyID, String pageNo, String loggedin_user_id, IResult resultCallback, String searchFilter, String searchkeyword) {
        //  Log.e( "MSG PSSED-", "pgno--" + pageNo + "userid---" + loggedin_user_id + "sb_cat--" + subCatergoryKeyID );
        mResultCallback = resultCallback;
        mContext = context;
        this.subCatergoryKeyID = subCatergoryKeyID;
        this.pageNo = pageNo;
        this.loggedin_user_id = loggedin_user_id;
        this.searchFilter = searchFilter;
        this.searchkeyword = searchkeyword;
    }

    public VolleyService(IResult resultCallback, Context context, String displayName_usrid, String username_or_msgid, String password_or_userid, String ConfrmPswd) {
        mResultCallback = resultCallback;
        mContext = context;
        // deviceToken,android_id,"android",signupuserID
        this.displayName_usrid = displayName_usrid;
        this.username_or_msgid = username_or_msgid;
        this.password_or_userid = password_or_userid;
        this.ConfrmPswd = ConfrmPswd;
    }

    public VolleyService(IResult resultCallback, Context ctx, String main_id, String sb_id, String created_date, String msg, String title, String posted_by_user_id) {
        mResultCallback = resultCallback;
        mContext = ctx;
        this.main_id = main_id;
        this.sb_id = sb_id;
        this.msg = msg;
        this.title = title;
        this.posted_by_user_id = posted_by_user_id;
        this.created_date = created_date;

    }

    public VolleyService(IResult mResultCallback, Context ctx, String msg_id, String likeornot_Fav, String userId) {
        this.mResultCallback = mResultCallback;
        mContext = ctx;
        this.msg_id = msg_id;
        this.likeornot_Fav = likeornot_Fav;
        this.userId = userId;
    }

    public VolleyService(IResult mResultCallback, Context applicationContext, String signup_user_id) {
        this.mResultCallback = mResultCallback;
        mContext = applicationContext;
        this.userId = signup_user_id;
    }

    public VolleyService(IResult mResultCallback, Context ctx, String loged_in_userID, String u_name, String u_email, String u_gender, String u_bio) {
        this.mResultCallback = mResultCallback;
        mContext = ctx;
        this.loggedin_user_id = loged_in_userID;
        this.u_name = u_name;
        this.u_email = u_email;
        this.u_gender = u_gender;
        this.u_bio = u_bio;
    }


    public void postDataVolley(final int CurrentServerPositionHit, final String subCatergoryKey) {
        RequestQueue MyRequestQueue = Volley.newRequestQueue( mContext );
        StringRequest MyStringRequest = new StringRequest( Request.Method.POST, Constants.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e( "Mypost-Res--", response );
                try {

                    mResultCallback.notifySuccess( response );

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
            @Override
            public void onErrorResponse(VolleyError error) {
                mResultCallback.notifyError( error );
                error.getStackTrace();

            }
        } ) {
            protected Map<String, String> getParams() {
                Map<String, String> MyData = new HashMap<>();
                if (CurrentServerPositionHit == 0) {
                    MyData.put( "apikey", "GET_CAT_SUBCAT_LIST" );
                } else if (CurrentServerPositionHit == 1) {
                    MyData.put( "apikey", "GETALLMESSAGES" );
                    MyData.put( "subcat_id", subCatergoryKeyID );
                    MyData.put( "page", pageNo );
                    MyData.put( "loggedin_user_id", loggedin_user_id );
                    MyData.put( "filter_key", searchFilter );
                    MyData.put( "search_key", searchkeyword );

                } else if (CurrentServerPositionHit == 2) {
                    MyData.put( "apikey", "GETALLUSER" );
                } else if (CurrentServerPositionHit == 3) {

                    MyData.put( "apikey", "POST_MESSAGE" );
                    MyData.put( "msg_title", title );
                    MyData.put( "category_id", main_id );
                    MyData.put( "subcat_id", sb_id );
                    MyData.put( "message", msg );
                    MyData.put( "created_at", created_date );
                    MyData.put( "posted_by", posted_by_user_id );


                } else if (CurrentServerPositionHit == 4) {
                    MyData.put( "apikey", "LOGIN" );
                    MyData.put( "username", username_or_msgid );
                    MyData.put( "pass", password_or_userid );
                } else if (CurrentServerPositionHit == 5) {
                    MyData.put( "apikey", "SIGNUP" );
                    MyData.put( "name", displayName_usrid );
                    MyData.put( "username", username_or_msgid );
                    MyData.put( "pass", password_or_userid );
                    MyData.put( "pass_confirmation", ConfrmPswd );


                } else if (CurrentServerPositionHit == 6) {
                    MyData.put( "apikey", "LIKEDISLIKE" );
                    MyData.put( "user_id", userId );
                    MyData.put( "message_id", msg_id );
                    MyData.put( "like_dislike", likeornot_Fav );


                } else if (CurrentServerPositionHit == 7) {
                    MyData.put( "apikey", "FAVUNFAV" );
                    MyData.put( "user_id", userId );
                    MyData.put( "message_id", msg_id );
                    MyData.put( "favourite", likeornot_Fav );


                } else if (CurrentServerPositionHit == 8) {
                    MyData.put( "apikey", "GETUSERINFO" );
                    MyData.put( "loggedin_id", userId );
                } else if (CurrentServerPositionHit == 9) {

                    MyData.put( "apikey", "UPDATEPROFILE" );
                    MyData.put( "loggedin_id", loggedin_user_id );
                    MyData.put( "name", u_name );
                    MyData.put( "email", u_email );
                    MyData.put( "gender", u_gender );
                    MyData.put( "about_me", u_bio );

                } else if (CurrentServerPositionHit == 10) {


                    MyData.put( "apikey", "DELETEPOST" );
                    MyData.put( "user_id", password_or_userid );
                    MyData.put( "message_id", username_or_msgid );


                } else if (CurrentServerPositionHit == 11) {
                    //my post Fragment
                    Log.e( "MYPOST-USER-ID-", "" + loged_in_userID + "--page--" + defaulPageClick + "" + "seach--" + searchFilter );
                    MyData.put( "apikey", "GETUSERMESSAGES" );
                    MyData.put( "loggedin_user_id", loged_in_userID );
                    MyData.put( "page", defaulPageClick );
                    MyData.put( "filter_key", searchFilter );
                    MyData.put( "search_key", searchkeyword );
                } else if (CurrentServerPositionHit == 12) {
                    //my Favourite Post
                    Log.e( "USER-ID-", "" + loged_in_userID + "--page--" + defaulPageClick + "" + "seach--" + searchFilter );
                    MyData.put( "apikey", "GETMYFAVPOST" );
                    MyData.put( "loggedin_user_id", loged_in_userID );
                    MyData.put( "page", defaulPageClick );
                    MyData.put( "filter_key", searchFilter );
                    MyData.put( "search_key", searchkeyword );
                } else if (CurrentServerPositionHit == 13) {

                    MyData.put( "apikey", "REPORTABUSE" );
                    MyData.put( "user_id", userId );
                    MyData.put( "message_id", msg_id );
                    MyData.put( "report_abuse", likeornot_Fav );
                } else if (CurrentServerPositionHit == 14) {

                    MyData.put( "apikey", "ADDDEVICE" );
                    MyData.put( "token_id", displayName_usrid );
                    MyData.put( "device_id", username_or_msgid );
                    MyData.put( "device_type", password_or_userid );
                    MyData.put( "user_id", ConfrmPswd );
                } else if (CurrentServerPositionHit == 15) {

                    MyData.put( "apikey", "FORGOTPASSWORD" );
                    MyData.put( "username", msg_id );
                    MyData.put( "newpass", likeornot_Fav );
                    MyData.put( "confirm_password", userId );

                } else if (CurrentServerPositionHit == 16) {

                    MyData.put( "apikey", "CHANGEPASSWORD" );
                    MyData.put( "user_id", displayName_usrid );
                    MyData.put( "old_pass", username_or_msgid );
                    MyData.put( "newpass", password_or_userid );
                    MyData.put( "confirm_password", ConfrmPswd );

                }


                return MyData;
            }
        };
        MyRequestQueue.add( MyStringRequest );


    }


}