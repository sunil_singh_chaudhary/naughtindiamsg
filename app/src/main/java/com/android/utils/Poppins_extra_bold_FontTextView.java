package com.android.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class Poppins_extra_bold_FontTextView extends AppCompatTextView {
    public static Typeface FONT_NAME;


    public Poppins_extra_bold_FontTextView(Context context) {
        super(context);
        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-ExtraBold.ttf");
        this.setTypeface(FONT_NAME);
    }
    public Poppins_extra_bold_FontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-ExtraBold.ttf");
        this.setTypeface(FONT_NAME);
    }
    public Poppins_extra_bold_FontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if(FONT_NAME == null) FONT_NAME = Typeface.createFromAsset(context.getAssets(), "fonts/Poppins-ExtraBold.ttf");
        this.setTypeface(FONT_NAME);
    }
}