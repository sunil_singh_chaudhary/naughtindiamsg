package com.android.utils;

import com.android.volley.VolleyError;

public interface IResult {

    public void notifySuccess(String response);
    public void notifyError(VolleyError error);


}
