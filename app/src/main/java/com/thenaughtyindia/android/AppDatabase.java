package com.thenaughtyindia.android;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.android.utils.MyNotiModel;

@Database(entities = {MyNotiModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
}