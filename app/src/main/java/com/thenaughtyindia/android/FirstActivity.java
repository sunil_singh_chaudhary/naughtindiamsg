package com.thenaughtyindia.android;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.fragments.CategoryFragment;
import com.android.fragments.FavouriteFragment;
import com.android.fragments.MyPostFragment;
import com.android.fragments.SettingFragment;
import com.android.utils.Constants;
import com.android.utils.IResult;
import com.android.utils.ToasterUtil;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.thenaughtyindia.android.LoadingScreenView.CustomProgressDialog;
import com.thenaughtyindia.android.databinding.ActivityFirstBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import am.appwise.components.ni.NoInternetDialog;


public class FirstActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener ,ToasterUtil.onclickListnerDialog{
    private IResult mResultCallback;
    private Multimap<String, String> multiMaphsmap = ArrayListMultimap.create();
    private Multimap<String, String> multiMaphsmap_description = ArrayListMultimap.create();
    static HashMap<String, String> subcategoryID_HasmapList = new HashMap<>();
    static HashMap<String, String> subcategory_description_HasmapList = new HashMap<>();
    private ArrayList<String> id_list = new ArrayList<>();
    private ArrayList<String> name_list = new ArrayList<>();
    private NoInternetDialog noInternetDialog;
    private CustomProgressDialog customdialog;
    private int currentServer = 0;
    private FragmentTransaction transaction;
    private FragmentManager fragmentManager;
    private FragmentManager fm;
    private ActivityFirstBinding firstBinding;
    private int lastSelectedItemId;
    private MenuItem menuItem;
    private int count;
    private ToasterUtil toasterUtil;
    private Fragment mCategoryFragment = new CategoryFragment();
    private Fragment mFavouriteFragment = new FavouriteFragment();
    private Fragment mMyPostFragment = new MyPostFragment();
    private Fragment mSettingFragment = new SettingFragment();
    @Override
    protected void onStart() {
        toasterUtil = new ToasterUtil( this );
        // getOnBackPressedDispatcher().addCallback( FirstActivity.this, onBackPressedCallback );

        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        firstBinding = ActivityFirstBinding.inflate( getLayoutInflater() );
        View view = firstBinding.getRoot();
        setContentView( view );
        SetActioBarText( getString( R.string.label_actionbar_appname ) );

        fragmentManager = getSupportFragmentManager();

        firstBinding.navigation.setItemIconTintList( null );
        firstBinding.navigation.setOnNavigationItemSelectedListener( this );

        init();


    }

    @Override
    protected void onResume() {
        checkInternetAvailibility();
        super.onResume();
    }

    private void checkInternetAvailibility() {
        noInternetDialog = new NoInternetDialog.Builder( FirstActivity.this ).build();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }


    public void init() {
        customdialog = new CustomProgressDialog();
        customdialog.show( FirstActivity.this, getString( R.string.wait ) );
        try {
            initVolleyCallback();
            VolleyService mVolleyService = new VolleyService( mResultCallback, FirstActivity.this );
            mVolleyService.postDataVolley( currentServer, "" );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//backpress work on after screen off and on again work fine but have some more problem in this
 @Override
    public void onBackPressed() {

       fm = getSupportFragmentManager();
        Log.e( "FirstActivity-- ", "back--"+fm.getBackStackEntryCount() );
        if (fm.getBackStackEntryCount() <= 0) {
            ActivityCompat.finishAffinity( FirstActivity.this );
        }else {
            getOnBackPressedDispatcher().onBackPressed();
        }

        count = fm.getBackStackEntryCount();
        Log.e( "count ", "--" + count );
        if (count == 0) {
            ActivityCompat.finishAffinity( FirstActivity.this );
            //additional code
        } else {

            getOnBackPressedDispatcher().onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        String valuestring;
        menuItem = item;
        if (item.getItemId() == lastSelectedItemId) { // added this
            Log.e( "both id is same", "LULL" );
            return true;
        }
        switch (item.getItemId()) {
            case R.id.navigation_category:

                SetActioBarText( getString( R.string.label_actionbar_categories ) );
                fragment = mCategoryFragment;
                valuestring = "category";
                break;

            case R.id.navigation_favourite:
                SetActioBarText( getString( R.string.label_actionbar_favourites ) );
                fragment = mFavouriteFragment;
                valuestring = "favourites";
                break;

            case R.id.navigation_mypost:
                SetActioBarText( getString( R.string.label_actionbar_post ) );
                fragment = mMyPostFragment;
                valuestring = "post";
                break;

            case R.id.navigation_setting:
                SetActioBarText( getString( R.string.label_actionbar_settinggs ) );
                fragment = mSettingFragment;
                valuestring = "setings";
                break;
            default:
                throw new IllegalStateException( "Unexpected value: " + menuItem.getItemId() );
        }

        return loadFragment( fragment, valuestring, menuItem );
    }

    private void SetActioBarText(String text) {
        getSupportActionBar().setDisplayOptions( ActionBar.DISPLAY_SHOW_CUSTOM );
        getSupportActionBar().setCustomView( R.layout.abs_layout );
        AppCompatTextView textView = findViewById( R.id.tvTitle );
        textView.setText( text );
    }

    private boolean loadFragment(Fragment fragment, String argument, MenuItem item) {
        //switching fragment
        if (fragment != null) {
            Bundle args = new Bundle();
            transaction = fragmentManager.beginTransaction();
            args.putSerializable( Constants.NAME_LIST_KEY, name_list );
            args.putSerializable( Constants.IDLIST_CAT_KEY, id_list );
            args.putSerializable( Constants.MULTIMAP_HASMAP_CAT_KEY, (Serializable) multiMaphsmap );
            args.putSerializable( Constants.SUBCAT_HASMAP_ID_CAT_KEY, subcategoryID_HasmapList );
            args.putSerializable( Constants.MULTIMAP_DESCRIPTION_KEY, (Serializable) multiMaphsmap_description );
            fragment.setArguments( args );
            transaction.addToBackStack( argument );
            transaction.setTransition( FragmentTransaction.TRANSIT_FRAGMENT_FADE );
            transaction.replace( R.id.frame_container, fragment, "demofragment" ).commitAllowingStateLoss();
            lastSelectedItemId = item.getItemId();
            Log.e( "menu itme id--", lastSelectedItemId + "" );

            return true;
        }
        return false;
    }



    // when using this method OnBackPressedCallback it giving double backpress error when screen
    // goes off and  then on otherwise it is working fine

/*    OnBackPressedCallback onBackPressedCallback = new OnBackPressedCallback( true ) {
        boolean doubleBackToExitPressedOnce = false;

        @Override
        public void handleOnBackPressed() {

            if (doubleBackToExitPressedOnce) {
                ActivityCompat.finishAffinity( FirstActivity.this );
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText( FirstActivity.this, getString( R.string.exitapp ), Toast.LENGTH_SHORT ).show();

            new Handler().postDelayed( new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000 );


        }
    };*/

    void initVolleyCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                JSONObject heroObject = null;
                name_list.clear();
                id_list.clear();
                subcategoryID_HasmapList.clear();
                multiMaphsmap.clear();
                multiMaphsmap_description.clear();
                subcategory_description_HasmapList.clear();

                try {
                    obj = new JSONObject( response );
                    JSONArray responseData = obj.getJSONArray( Constants.GET_RESPONSE_FRM_SERVER );
                    for (int i = 0; i < responseData.length(); i++) {
                        heroObject = responseData.getJSONObject( i );
                        String cat_name = heroObject.getString( Constants.CATEGORY_NAME );
                        name_list.add( cat_name );
                        String id = heroObject.getString( Constants.ID );
                        id_list.add( id );

                        JSONArray subcatarray = heroObject.getJSONArray( Constants.SUB_CAT_LIST );
                        for (int j = 0; j < subcatarray.length(); j++) {
                            JSONObject suboject = subcatarray.getJSONObject( j );

                            String sub_category_id = suboject.getString( Constants.CATEGORY_ID );
                            Log.e( "sub_category_id-", sub_category_id );

                            String subcatname = suboject.getString( Constants.SUB_CAT_NAME );
                            Log.e( "Sub cat name-", subcatname );

                            String sub_description = suboject.getString( Constants.DESCRIPTION );
                            Log.e( "descr", sub_description );

                            String subid = suboject.getString( Constants.ID );
                            Log.e( "subid", subid );

                            subcategoryID_HasmapList.put( subid, subcatname );

                            subcategory_description_HasmapList.put( subid, sub_description );

                            //for subcatergory subcatName mapping
                            HaspMapSetMethod( sub_category_id, subcatname );

                            //for subcatergory description mapping
                            HaspMapSetSub_description( sub_category_id, sub_description );

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                customdialog.dialog.dismiss();
                //check Intro Screen Runs Before or NotF
                CategoryFragment fragment = new CategoryFragment();
                loadFragment( fragment, "category", menuItem );

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtil.showToast( Constants.NETWORK_ERROR,FirstActivity.this );

                } else if (error instanceof ServerError) {
                    toasterUtil.showToast( Constants.SERVER_ERROR,FirstActivity.this );

                } else if (error instanceof NoConnectionError) {
                    toasterUtil.showToast( Constants.NO_INTERNET_CONNECTION,FirstActivity.this );

                } else if (error instanceof TimeoutError) {
                    toasterUtil.showToast( Constants.CONNECTION_TIME_OUT,FirstActivity.this );

                } else {
                    toasterUtil.showToast( Constants.UNKNOWN_ERROR,FirstActivity.this );

                }
            }
        };
    }
    @Override
    public void onClick(View view, Boolean clicktruefalse) {
        if (clicktruefalse){
            customdialog.dialog.dismiss();
            init();
        }

    }


    private void HaspMapSetMethod(String category_id, String subcatname) {
        multiMaphsmap.put( category_id, subcatname );


    }

    private void HaspMapSetSub_description(String category_id, String subcatname) {
        multiMaphsmap_description.put( category_id, subcatname );

    }


}