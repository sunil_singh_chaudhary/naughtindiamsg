package com.thenaughtyindia.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.utils.Constants;
import com.android.utils.FileUtils;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.android.utils.ToasterUtil;
import com.android.utils.ToasterUtilsecond;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.thenaughtyindia.android.LoadingScreenView.CustomProgressDialog;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileEditorActivity extends AppCompatActivity implements ToasterUtil.onclickListnerDialog {

    private ImageView edit_profile, save_profile;
    private EditText user_name_info, email_info_textv, gender_info_textv, bio_info;
    private int GETALL_USER_INFO_CURRENT_SERVER = 8;
    private int SETALL_USER_INFO_CURRENT_SERVER = 9;
    private SignUpPrefManager signUpActivity;
    private TextView total_post_textv, total_fav_textv;
    private IResult mResultCallback;
    private CustomProgressDialog customProgressDialog;
    private CircleImageView profile_imgv;
    private FileUtils fileUtils;
    private ToasterUtil  toasterUtil;
    private ToasterUtilsecond toasterUtilsecond;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile_editor );
        toasterUtil = new ToasterUtil( this );
        signUpActivity = new SignUpPrefManager( this );
        fileUtils = new FileUtils( ProfileEditorActivity.this );
        init();
        showVollyGetAllUserInfo();
    }

    private void showVollyGetAllUserInfo() {

        customProgressDialog = new CustomProgressDialog();
        customProgressDialog.show( ProfileEditorActivity.this );
        initVolleyGetAllUserInfoCallback();
        try {
            VolleyService mVolleyService = new VolleyService( mResultCallback, getApplicationContext(), signUpActivity.loged_in_userID() );
            mVolleyService.postDataVolley( GETALL_USER_INFO_CURRENT_SERVER, "" );


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void initVolleyGetAllUserInfoCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {

                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    obj = new JSONObject( response );
                    heroObject = obj.getJSONObject( Constants.GET_RESPONSE_FRM_SERVER );
                    String user_id = heroObject.getString( Constants.USER_ID );
                    String user_type = heroObject.getString( Constants.USER_TYPE );
                    String age = heroObject.getString( Constants.AGE );

                    String name = heroObject.getString( Constants.NAME );
                    String email = heroObject.getString( Constants.EMAIL );
                    String profile_photo = heroObject.getString( Constants.PROFILE_PHOTO );
                    String gender = heroObject.getString( Constants.GENDER );
                    String about_me = heroObject.getString( Constants.ABOUT_ME );
                    String my_total_post_count = heroObject.getString( Constants.MY_TOTAL_POST_COUNT );
                    String my_total_favourite_count = heroObject.getString( Constants.MY_TOTAL_FAVOURITE_COUNT );
                    if (name.equals( "null" ) || name.equals( "" )) {
                        user_name_info.setText( R.string.namenotspecified );
                    } else {
                        user_name_info.setText( name );
                    }

                    if (email.equals( "null" )) {
                        email_info_textv.setText( R.string.emailnotspecified );
                    } else {
                        email_info_textv.setText( email );
                    }

                    //--------- save data for prefernce---------
                    signUpActivity.setprofilePhoto( profile_photo );
                    //-----------------------------------------

                    Glide.with( getApplicationContext() )
                            .load( profile_photo ) // Remote URL of image.
                            .placeholder( R.drawable.app_icon )
                            .into( profile_imgv );

                    if (gender.equals( "null" )) {
                        gender_info_textv.setText( R.string.generedernotspecified );
                    } else {
                        gender_info_textv.setText( gender );
                    }

                    Log.e( "gender", gender + "" );

                    if (about_me.equals( "null" )) {
                        bio_info.setText( R.string.bionotfiond );
                    } else {
                        bio_info.setText( about_me );
                    }


                    total_post_textv.setText( my_total_post_count + "" );
                    total_fav_textv.setText( my_total_favourite_count + "" );

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //check Intro Screen Runs Before or Not
                customProgressDialog.dialog.dismiss();
            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtil.showToast( Constants.NETWORK_ERROR, ProfileEditorActivity.this );

                } else if (error instanceof ServerError) {
                    toasterUtil.showToast( Constants.SERVER_ERROR,ProfileEditorActivity.this);

                } else if (error instanceof NoConnectionError) {
                    toasterUtil.showToast( Constants.NO_INTERNET_CONNECTION,ProfileEditorActivity.this );

                } else if (error instanceof TimeoutError) {
                    toasterUtil.showToast( Constants.CONNECTION_TIME_OUT,ProfileEditorActivity.this );

                } else {
                    toasterUtil.showToast( Constants.UNKNOWN_ERROR,ProfileEditorActivity.this );

                }
            }

        };
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
        // check if the request code is same as what is passed  here it is 2
        Log.e( "result", resultCode + "" );
        if (resultCode == RESULT_OK) {
            Uri fileUri = data.getData();
            Glide.with( ProfileEditorActivity.this )
                    .load( fileUri ) // Remote URL of image.
                    .into( profile_imgv );

            //converting image to base64 string
            Bitmap bm = BitmapFactory.decodeFile( fileUtils.getPath( fileUri ) );

            //save to server image
            fileUtils.uploadBitmap( bm, getApplicationContext(), signUpActivity );

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText( this,R.string.error, Toast.LENGTH_SHORT ).show();
        } else {
            Toast.makeText( this, R.string.canceld, Toast.LENGTH_SHORT ).show();
        }
    }

    private void init() {

        user_name_info = findViewById( R.id.user_name_info );
        email_info_textv = findViewById( R.id.email_info_textv );
        gender_info_textv = findViewById( R.id.gender_info_textv );
        bio_info = findViewById( R.id.bio_info );
        total_post_textv = findViewById( R.id.total_post_textv );
        total_fav_textv = findViewById( R.id.total_fav_textv );
        profile_imgv = findViewById( R.id.profile );
        edit_profile = findViewById( R.id.edit_profile );

        save_profile = findViewById( R.id.save_profile );
        profile_imgv.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImagePicker.Companion.with( ProfileEditorActivity.this )
                        .crop()                    //Crop image(Optional), Check Customization for more option
                        .compress( 1004 )            //Final image size will be less than 1 MB(Optional)
                        .maxResultSize( 400, 400 )    //Final image resolution will be less than 1080 x 1080(Optional)
                        .start();
            }
        } );

        save_profile.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService( Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                user_name_info.setFocusable( false );
                user_name_info.setFocusableInTouchMode( false );
                email_info_textv.setFocusable( false );
                email_info_textv.setFocusableInTouchMode( false );
                gender_info_textv.setFocusable( false );
                gender_info_textv.setFocusableInTouchMode( false );

                bio_info.setFocusable( false );
                bio_info.setFocusableInTouchMode( false );
                Log.e( "CALL", "save" );
                save_profile.setVisibility( View.GONE );
                edit_profile.setVisibility( View.VISIBLE );


                showVollySaveUserInfo( user_name_info.getText().toString(), email_info_textv.getText().toString(), gender_info_textv.getText().toString(), bio_info.getText().toString() );

            }
        } );
        edit_profile.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e( "CALL", "edit" );

                user_name_info.setFocusable( true );
                user_name_info.setFocusableInTouchMode( true );
                user_name_info.requestFocus();

                email_info_textv.setFocusableInTouchMode( true );
                gender_info_textv.setFocusableInTouchMode( true );
                bio_info.setFocusableInTouchMode( true );

                save_profile.setVisibility( View.VISIBLE );
                edit_profile.setVisibility( View.GONE );

                InputMethodManager imm = (InputMethodManager) getSystemService( Context.INPUT_METHOD_SERVICE );
                imm.showSoftInput( user_name_info, InputMethodManager.SHOW_FORCED );

            }
        } );


    }

    private void showVollySaveUserInfo(String u_name, String u_email, String u_gender, String u_bio) {
        customProgressDialog = new CustomProgressDialog();
        customProgressDialog.show( ProfileEditorActivity.this );
        initVolleySetAllUserInfoCallback();
        try {
            VolleyService mVolleyService = new VolleyService( mResultCallback, getApplicationContext(), signUpActivity.loged_in_userID(), u_name, u_email, u_gender, u_bio );
            mVolleyService.postDataVolley( SETALL_USER_INFO_CURRENT_SERVER, "" );


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void initVolleySetAllUserInfoCallback() {
        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {


                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    obj = new JSONObject( response );
                    heroObject = obj.getJSONObject( Constants.GET_RESPONSE_FRM_SERVER );
                    String user_id = heroObject.getString( Constants.USER_ID );


                } catch (Exception e) {
                    e.printStackTrace();
                }
                customProgressDialog.dialog.dismiss();
                //check Intro Screen Runs Before or Not

            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtilsecond.showToast( Constants.NETWORK_ERROR);

                } else if (error instanceof ServerError) {
                    toasterUtilsecond.showToast( Constants.SERVER_ERROR);

                } else if (error instanceof NoConnectionError) {
                    toasterUtilsecond.showToast( Constants.NO_INTERNET_CONNECTION );

                } else if (error instanceof TimeoutError) {
                    toasterUtilsecond.showToast( Constants.CONNECTION_TIME_OUT);

                } else {
                    toasterUtilsecond.showToast( Constants.UNKNOWN_ERROR);

                }
            }
        };
    }


    @Override
    public void onClick(View view, Boolean clicktruefalse) {
        if (clicktruefalse) {
            showVollyGetAllUserInfo();
        }
    }
}