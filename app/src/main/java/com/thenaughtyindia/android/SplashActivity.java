package com.thenaughtyindia.android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.android.intro.PrefManager;
import com.android.intro.WelcomeActivity;

public class SplashActivity extends AppCompatActivity {

    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash );
        prefManager = new PrefManager( this );
        IntroFirstTimeOrNot();

    }


    private void IntroFirstTimeOrNot() {

        if (prefManager.isFirstTimeLaunch()) {
            Intent intent = new Intent( SplashActivity.this, WelcomeActivity.class );
            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity( intent );
            Log.e( "LAUNCH", "First time" );
        } else {
            Intent intent = new Intent( SplashActivity.this, FirstActivity.class );
            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity( intent );

        }

    }

}

