package com.thenaughtyindia.android;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.android.adapters.NotificatioinAdapter;
import com.android.utils.Constants;
import com.android.utils.IResult;
import com.android.utils.SignUpPrefManager;
import com.android.utils.ToasterUtil;
import com.android.utils.VolleyService;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NotificationFirebaseActivity extends AppCompatActivity implements ToasterUtil.onclickListnerDialog {
    private SignUpPrefManager signUpPrefManager;
    private IResult mResultCallback;
    private String deviceToken, android_id, signupuserID;
    private ToasterUtil toasterUtil;
    private int FirebaseConstantServerValue = 14;
    private AppDatabase db;
    private RecyclerView mRecycler;
    TextView notification_textv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_rockstar );
        toasterUtil = new ToasterUtil( this );
        signUpPrefManager = new SignUpPrefManager( NotificationFirebaseActivity.this );
        mRecycler = findViewById( R.id.notificatioin_recyclerview );
        LinearLayoutManager mLayoutManager = new LinearLayoutManager( getApplicationContext() );
        mRecycler.setLayoutManager( mLayoutManager );
        notification_textv = findViewById( R.id.notification_textv );
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener( new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new FCM registration token
                        deviceToken = task.getResult();
                        android_id = Settings.Secure.getString( getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID );
                        init( deviceToken, android_id );

                    }
                } );


        db = Room.databaseBuilder( getApplicationContext(), AppDatabase.class, "MyNotiModel" ).build();

        Intent myFirebaseMessagingService = new Intent( this, MyFirebaseMessagingService.class );
        startService( myFirebaseMessagingService );

        if (signUpPrefManager.getwitchStatus())
            new getDataFromRoomDatabaseAsyncTask( this, db ).execute();


    }

    public void init(String deviceToken, String android_id) {
        this.deviceToken = deviceToken;
        this.android_id = android_id;
        if (signUpPrefManager.loged_in_userID().equals( "0" )) {
            signupuserID = "";

        } else {
            signupuserID = signUpPrefManager.loged_in_userID();
        }
        Log.e( "SIGNI OR NOT-", signupuserID );
        initVolleyCallbackNotification();
        VolleyService mVolleyService = new VolleyService( mResultCallback, NotificationFirebaseActivity.this, deviceToken, android_id, "android", signupuserID );
        mVolleyService.postDataVolley( FirebaseConstantServerValue, "" );
    }

    private void initVolleyCallbackNotification() {

        mResultCallback = new IResult() {
            @Override
            public void notifySuccess(String response) {
                JSONObject obj = null;
                JSONObject heroObject = null;
                try {
                    Log.e( "responese---", response );

                    obj = new JSONObject( response );


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void notifyError(VolleyError error) {
                error.printStackTrace();
                if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volleyError = new VolleyError( new String( error.networkResponse.data ) );
                    volleyError.printStackTrace();
                }

                if (error instanceof NetworkError) {
                    toasterUtil.showToast( Constants.NETWORK_ERROR, NotificationFirebaseActivity.this );

                } else if (error instanceof ServerError) {
                    toasterUtil.showToast( Constants.SERVER_ERROR, NotificationFirebaseActivity.this );

                } else if (error instanceof NoConnectionError) {
                    toasterUtil.showToast( Constants.NO_INTERNET_CONNECTION, NotificationFirebaseActivity.this );

                } else if (error instanceof TimeoutError) {
                    toasterUtil.showToast( Constants.CONNECTION_TIME_OUT, NotificationFirebaseActivity.this );

                } else {
                    toasterUtil.showToast( Constants.UNKNOWN_ERROR, NotificationFirebaseActivity.this );

                }
            }
        };


    }


    @Override
    public void onClick(View view, Boolean clicktruefalse) {
        if (clicktruefalse) {
            init( deviceToken, android_id );
        }
    }

    public class getDataFromRoomDatabaseAsyncTask extends AsyncTask<Void, Void, Void> {
        private String title, msgs;
        private WeakReference<Activity> weakActivity;
        private AppDatabase db;
        private ArrayList<String> adding_msg_list = new ArrayList<>();
        private ArrayList<String> jsonformateStringfromserverList = new ArrayList<>();
        private UserDao userDao;
        private List<String> msg_list;

        public getDataFromRoomDatabaseAsyncTask(Activity activity, AppDatabase db) {
            weakActivity = new WeakReference<>( activity );
            this.db = db;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            userDao = db.userDao();
            msg_list = userDao.getAllmsgList();
            Log.e( "SIZE", msg_list.size() + "" );
            if (msg_list.isEmpty()) {
                mRecycler.setVisibility( View.GONE );
                notification_textv.setVisibility( View.VISIBLE );

            } else {
                mRecycler.setVisibility( View.VISIBLE );
                notification_textv.setVisibility( View.GONE );
                for (int i = 0; i <= msg_list.size(); i++) {
                    try {
                        String msg_substring = msg_list.get( i ).substring( msg_list.get( i ).indexOf( "message=" ) + 8 );
                        Log.e( "msg_substring---", msg_substring );
                        adding_msg_list.add( msg_substring );

                        //send full json formatae so to delete from database comparision
                        jsonformateStringfromserverList.add( msg_list.get( i ) );

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Activity activity = weakActivity.get();
            if (activity == null) {
                return;
            }
            Log.e( "LIST SIZE", adding_msg_list.size() + "" );
            mRecycler.setAdapter( new NotificatioinAdapter( NotificationFirebaseActivity.this, adding_msg_list, jsonformateStringfromserverList ) );
            super.onPostExecute( aVoid );
        }
    }

    @Override
    public void onBackPressed() {
        startActivity( new Intent( NotificationFirebaseActivity.this, FirstActivity.class ));

        super.onBackPressed();
    }
}

