package com.thenaughtyindia.android;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.android.utils.MyNotiModel;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT msg FROM MyNotiModel")
    List<String> getAllmsgList();


    @Insert
    void insertAll(MyNotiModel... users);

    @Delete
    void delete(MyNotiModel user);

    @Query("DELETE FROM MyNotiModel WHERE msg = :msg")
    void deleteBymsg(String msg);
}